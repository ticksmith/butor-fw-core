/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao;

import org.butor.utils.Message;
import org.butor.utils.Message.MessageType;
import org.butor.utils.MessageID;

public enum DAOMessageID implements MessageID {
	SQL_EXCEPTION(MessageType.ERROR),
	DAO_FAILURE(MessageType.ERROR),
	DELETE_FAILURE(MessageType.ERROR),
	INSERT_FAILURE(MessageType.ERROR),
	UPDATE_FAILURE(MessageType.ERROR),
	UNAUTHORIZED(MessageType.ERROR);

	private final static String SYSID = "dao";
	
	private final MessageType type;
	private final Message messageObject;
	
	private DAOMessageID(MessageType type) {
		this.type = type;
		this.messageObject =new Message(this); 
	}
	
	public Message getMessage() {
		return messageObject;
	}
	
	public Message getMessage(String message) {
		return new Message(this, message);
	}
	
	@Override
	public String getId() {
		return name();
	}


	@Override
	public String getSysId() {
		return SYSID;
	}

	@Override
	public MessageType getMessageType() {
		return type;
	}
}
