/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.sql.DataSource;
import javax.transaction.Status;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import com.google.common.base.Preconditions;

public class DataSourceProxy implements FactoryBean<DataSource>, InvocationHandler, BeanFactoryAware, InitializingBean {
	private DataSource xaDataSource;
	private DataSource dataSource;
	private TransactionManager transactionManager;
	private DataSource proxy;
	private BeanFactory beanFactory; 

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public DataSource getObject() throws Exception {
		if(proxy == null) {
			proxy = (DataSource)Proxy.newProxyInstance(
				getClass().getClassLoader(), 
				new Class<?>[] { DataSource.class }, 
				this
			);
		}
		return proxy;
	}

	@Override
	public Class<?> getObjectType() {
		return DataSource.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public Object invoke(
		final Object proxy_, 
		final Method method_, 
		final Object[] args_) throws Throwable {

		DataSource ds = null;

		if(isXa()) {
			ds = xaDataSource;
		} else {
			ds = dataSource;
		}

		return method_.invoke(ds, args_);
	}

	private boolean isXa() {
		if(transactionManager == null)
			return false;

		try {
			Transaction tx = transactionManager.getTransaction();
			if(tx == null)
				return false;

			boolean inTransaction = tx.getStatus() != Status.STATUS_NO_TRANSACTION;
			Preconditions.checkArgument(!(xaDataSource == null && inTransaction),"In a XA Transaction but no XA Datasource available!");
			return inTransaction;
		} catch(Throwable e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory=beanFactory;
		
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Preconditions.checkNotNull(dataSource,"DataSource is mandatory!");
		try {
			transactionManager = beanFactory.getBean(TransactionManager.class);
		} catch (NoSuchBeanDefinitionException e) {
			logger.warn("No (or more than one) TransactionManager defined in your Spring context. Will not set transaction manager.",e);
		}
	}

	public void setXaDataSource(DataSource xaDataSource) {
		this.xaDataSource = xaDataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
