/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao;

import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;

/**
 * @deprecated I believe it's better to let the super class do the job...
 * 
 */
@Deprecated
public class DefaultDaoExceptionTranslator extends SQLErrorCodeSQLExceptionTranslator {

	@Override
	public DataAccessException customTranslate(String task_, String sql_, SQLException e_) {
		/** here we translate to our specific exception if required
		if (e_.getErrorCode() == -12345) {
            return new DeadlockLoserDataAccessException(task_, e_);
        }
		*/
		
		return new DaoException(String.format("Failed to execute %s with SQL=%s", task_, sql_), e_);
	}

}
