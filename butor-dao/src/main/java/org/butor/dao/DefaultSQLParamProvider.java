/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao;

import java.lang.reflect.Field;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.google.common.base.Throwables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class DefaultSQLParamProvider implements SQLParamProvider {

	private boolean warnParamOverride=true;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	
	
	@SuppressWarnings("unchecked")
	@Override
	public MapSqlParameterSource prepParams(Object... args) {

		MapSqlParameterSource msp = new MapSqlParameterSource();
		for (Object arg : args) {
			if (null == arg) {
				continue;
			}
			
			if (arg instanceof Map) {
				msp.addValues((Map<String,?>) arg);
				continue;
			}
			if (arg instanceof MapSqlParameterSource) {
				msp.addValues(((MapSqlParameterSource) arg).getValues());
				continue;
			}

			BeanPropertySqlParameterSource sps = new BeanPropertySqlParameterSource(arg);
			List<Field> fields = new ArrayList<Field>();
			for (Class<?> c = arg.getClass(); c != null; c = c.getSuperclass()) {
				if (c.equals(Object.class))
					continue;
				fields.addAll(Arrays.asList(c.getDeclaredFields()));
			}
			for (Field f : fields) {
				try {
					String fn = f.getName();
					if (msp.hasValue(fn)) {
						if (warnParamOverride) {
							warnParamOverride = false;
							logger.warn(String
									.format("Field with name=%s has "
											+ "been already mapped by another arg bean. Overriding! Next time will warn if DEBUG is enabled.",
											fn));
						} else {
							if (logger.isDebugEnabled()) {
								logger.warn(String.format("Field with name=%s has "
										+ "been already mapped by another arg bean. Overriding!", fn));
							}
						}
					}
					if (Enum.class.isAssignableFrom(f.getType())) {
						sps.registerSqlType(f.getName(), Types.VARCHAR);
					}

					msp.addValue(fn, sps.getValue(fn), sps.getSqlType(fn), sps.getTypeName(fn));
					logger.debug(String.format("prepared sql arg: name=%s, value=%s, type=%s", fn, sps.getValue(fn),
							sps.getTypeName(fn)));
				} catch (Exception e) {
					Throwables.propagate(e);
				}
			}
		}
		return msp;
	}

	public void setWarnParamOverride(boolean warnParamOverride) {
		this.warnParamOverride = warnParamOverride;
	}

}
