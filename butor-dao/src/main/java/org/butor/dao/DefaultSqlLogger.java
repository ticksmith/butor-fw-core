/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.butor.dao.AbstractDao.UpdateResult;
import org.butor.utils.CommonDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterUtils;

public class DefaultSqlLogger implements SqlLogger {
	private Logger sqlLogger = LoggerFactory.getLogger("SQL");

	private boolean logArgsWithStats = false;
	
	@Override
	public void preQueryCall(String procName, String sql, MapSqlParameterSource args) {
		sqlLogger.info("Calling {}({}) ...", procName, formatArgs(args));
	}
	@Override
	public <T> void logQuery(String procName, String sql, MapSqlParameterSource args, boolean success, long elapsed,
			T result) {
		int rowCount = -1;
		if (result instanceof UpdateResult) {
			rowCount = ((UpdateResult) result).numberOfRowAffected;
		} else if (result instanceof Collection<?>) {
			rowCount = ((Collection<?>) result).size();
		} else if (result instanceof Number) {
			rowCount = ((Number)result).intValue();
		} else if (result != null) {
			rowCount = 1;
		}

		String argStr = formatArgs(args);
		String rc = rowCount == -1 ? "unknown" : rowCount +"";
		if (sqlLogger.isDebugEnabled()) {
			String parsedSql = generateQuery(sql, args);
			parsedSql = parsedSql.replaceAll("\\s", " ");
			sqlLogger.debug("{}({}): <SQL>{}</SQL>, success: {}, elapsed: {} ms, rowCount/rowAffected: {}",
				new Object[] {procName, argStr, parsedSql, success, elapsed, rc});
		} else if (sqlLogger.isInfoEnabled()) {
			sqlLogger.info("{}({}), success: {}, elapsed: {} ms, rowCount/rowAffected: {}", 
				new Object[] {procName, logArgsWithStats ? argStr : "...", success, elapsed, rc});
		}
	}
	private String formatArgs(MapSqlParameterSource args) {
		if (args == null) {
			return null;
		}
		StringWriter argStr = new StringWriter();
		argStr.append("{");
		Map<String,Object> argPairs = Collections.emptyMap(); 
		if (args != null) {
			argPairs = args.getValues();
		}
		int count=argPairs.size();
		int i=0;
		for (String key : argPairs.keySet()) {
			i++;
			Object val = argPairs.get(key);
			argStr.append("\"").append(key).append("\":");
			if (val==null) {
				argStr.append("null");
			} else if (val instanceof Collection<?>) {
				int j=0;
				Collection<?> list = (Collection<?>)val;
				argStr.append("[");
				for (Object item : list){
					argStr.append(formatValue(item));
					if(j<list.size() - 1){
						argStr.append(", ");
					}
					j++;
				}
				argStr.append("]");
			} else {
				argStr.append(formatValue(val));
			}
			if (i<count) {
				argStr.append(", ");
			}
		}
		argStr.append("}");
		return argStr.toString();
	}
	private String formatValue(Object v) {
		if (v == null) {
			return "NULL";
		}
		String value;
		if (v instanceof String) {
			value = "\"" + String.valueOf(v).replace("\"","\\\"") + "\"";
		} else if (v instanceof Enum<?>) {
			value = "\"" + String.valueOf(v) +"\"";
		} else if (v instanceof Date) {
			value = "\"" + CommonDateFormat.YYYYMMDD_HHMMSS_WITHMS.format((Date) v) + "\"";
		} else if (v instanceof Boolean) {
			value = ((Boolean)v ? "true" : "false");
		} else {
			value = String.valueOf(v);
		}
		return value;
	}
	protected String generateQuery(String sql, MapSqlParameterSource args) {
		if (sql == null) {
			return null;
		}
		String preparedSql = NamedParameterUtils.substituteNamedParameters(sql, args);
		List<?> valueList = Arrays.asList(NamedParameterUtils.buildValueArray(sql, args.getValues()));
		preparedSql = preparedSql.replaceAll("  ", " ");
		for (Object value : valueList) {
			preparedSql = prepareParsedSQLLog(preparedSql, value);
		}
		return preparedSql;
	}
	private String prepareParsedSQLLog(String parsedSql, Object v) {
		String replacedValue;
		if (v == null) {
			replacedValue = "NULL";
		} else if (v instanceof List) {
			for (Object o : (List<?>) v) {
				parsedSql = prepareParsedSQLLog(parsedSql, o);
			}
			return parsedSql;		
		} else if (v instanceof Enum<?>) {
			replacedValue = "\"" + v +"\"";
		} else if (v instanceof Date) {
			replacedValue = "\"" + CommonDateFormat.YYYYMMDD_HHMMSS_WITHMS.format((Date) v) + "\"";
		} else if (v instanceof Boolean) {
			replacedValue = ((Boolean)v ? "true" : "false");
		} else if (v instanceof String) {
			replacedValue = "\"" + String.valueOf(v).replace("\"","\\\"") +"\"";
		} else {
			replacedValue = String.valueOf(v);
		}

		int idx = parsedSql.indexOf('?');
		if (idx >= 0) {
			parsedSql = parsedSql.substring(0, idx) + replacedValue + parsedSql.substring(idx + 1);
		}
		return parsedSql;
	}
	public void setLogArgsWithStats(boolean logArgsWithStats) {
		this.logArgsWithStats = logArgsWithStats;
	}
}
