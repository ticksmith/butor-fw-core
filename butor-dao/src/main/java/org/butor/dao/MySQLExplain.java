/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao;


public class MySQLExplain {

	private int id;
	private String select_type;
	private String table;
	private String type;
	private String possible_key;
	private String key;	
	private Integer key_len;
	private String ref;
	private int rows;
	private String Extra;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Extra == null) ? 0 : Extra.hashCode());
		result = prime * result + id;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((key_len == null) ? 0 : key_len.hashCode());
		result = prime * result
				+ ((possible_key == null) ? 0 : possible_key.hashCode());
		result = prime * result + ((ref == null) ? 0 : ref.hashCode());
		result = prime * result + rows;
		result = prime * result
				+ ((select_type == null) ? 0 : select_type.hashCode());
		result = prime * result + ((table == null) ? 0 : table.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MySQLExplain other = (MySQLExplain) obj;
		if (Extra == null) {
			if (other.Extra != null)
				return false;
		} else if (!Extra.equals(other.Extra))
			return false;
		if (id != other.id)
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (key_len == null) {
			if (other.key_len != null)
				return false;
		} else if (!key_len.equals(other.key_len))
			return false;
		if (possible_key == null) {
			if (other.possible_key != null)
				return false;
		} else if (!possible_key.equals(other.possible_key))
			return false;
		if (ref == null) {
			if (other.ref != null)
				return false;
		} else if (!ref.equals(other.ref))
			return false;
		if (rows != other.rows)
			return false;
		if (select_type == null) {
			if (other.select_type != null)
				return false;
		} else if (!select_type.equals(other.select_type))
			return false;
		if (table == null) {
			if (other.table != null)
				return false;
		} else if (!table.equals(other.table))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "MySQLExplain [id=" + id + ", select_type=" + select_type
				+ ", table=" + table + ", type=" + type + ", possible_key="
				+ possible_key + ", key=" + key + ", key_len=" + key_len
				+ ", ref=" + ref + ", rows=" + rows + ", Extra=" + Extra + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSelect_type() {
		return select_type;
	}
	public void setSelect_type(String select_type) {
		this.select_type = select_type;
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPossible_key() {
		return possible_key;
	}
	public void setPossible_key(String possible_key) {
		this.possible_key = possible_key;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Integer getKey_len() {
		return key_len;
	}
	public void setKey_len(Integer key_len) {
		this.key_len = key_len;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getExtra() {
		return Extra;
	}
	public void setExtra(String extra) {
		Extra = extra;
	}


}
