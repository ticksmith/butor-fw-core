/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

public class SqlLoggerWithSqlGenerator extends DefaultSqlLogger implements SqlQueryGenerator {
	private Logger logger = LoggerFactory.getLogger(getClass());

	private Map<String,String> sqlMap = Collections.synchronizedMap(new HashMap<String,String>());

	@Override
	public <T> void logQuery(String procName, String sql, MapSqlParameterSource args, boolean success, long elapsed,
			T result) {
		super.logQuery(procName, sql, args, success, elapsed, result);
		if (!sqlMap.containsKey(procName)) {
			sqlMap.put(procName, sql);
		}
	}

	@Override
	public String generateQuery(String procName, Map<String, Object> args) {
		logger.info("Preparing query of proc {} with args {}", procName, args);
		String sql = sqlMap.get(procName);
		if (sql == null) {
			return null;
		}
		if (args != null) {
			MapSqlParameterSource msp = new MapSqlParameterSource();
			msp.addValues(args);
			sql = super.generateQuery(sql, msp);
		}
		logger.info("Prepared query of proc {} with args {}: {}", new Object[] {procName, args, sql});
		return sql;
	}
}
