/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao.extractor;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

public abstract class FilterResultSetExtractor<T> implements ResultSetExtractor<List<T>> {

	private RowMapper<T> _rowMapper = null;

	abstract boolean doStart(int index_);
	abstract boolean doStop(int index_);
	abstract boolean accept(Object row_, int index_);
	private static final List<Class<?>> singleFieldRsTypes = new ArrayList<Class<?>>(); 
	static {
		singleFieldRsTypes.add(String.class);
		singleFieldRsTypes.add(Date.class);
		singleFieldRsTypes.add(Integer.class);
		singleFieldRsTypes.add(Long.class);
		singleFieldRsTypes.add(Double.class);
		singleFieldRsTypes.add(Boolean.class);
		singleFieldRsTypes.add(BigDecimal.class);
		singleFieldRsTypes.add(Short.class);
		singleFieldRsTypes.add(Float.class);
		singleFieldRsTypes.add(java.sql.Date.class);
		singleFieldRsTypes.add(Byte.class);
	}

	public FilterResultSetExtractor(Class<T> resultSetClass_) {
		if (singleFieldRsTypes.contains(resultSetClass_)) {
			_rowMapper = new RowMapper<T>() {
				public T mapRow(ResultSet resultSet, int i) throws SQLException {
					return (T)resultSet.getObject(1);
				}
			};
		} else {
			_rowMapper = new BeanPropertyRowMapper<T>(resultSetClass_);
		}
	}
	@Override
	public List<T> extractData(ResultSet rs_) throws SQLException, DataAccessException {
		List<T> results = new ArrayList<T>();
		int index = 0;
		while (!doStart(index) && rs_.next()) {
			index++;
		}
		while (rs_.next()) {
			T row = _rowMapper.mapRow(rs_, index);
			if (accept(row, index)) {
				results.add(row);
			}
			if (doStop(index++))
				break;
		}
		return results;
	}
}
