/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.butor.dao.RowHandler;
import org.butor.dao.test.bean.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:test-context.xml")
public class TestDao {
	private static final String LIST_CLIENT_SQL = "select * from client where lastName like :lastName";
	private static Logger _logger = LoggerFactory.getLogger(TestDao.class);
	private static ClientDaoImpl _sdi = null;

	private static Long _insertedClientId = null;

	@Resource
	private DataSource ds;


	@Before
	public void setup() throws SQLException {
		String sql = "CREATE TABLE client (id INT NOT NULL GENERATED ALWAYS AS IDENTITY, " +
				"firstName VARCHAR(45) NOT NULL, " +
				"lastName VARCHAR(45) NOT NULL, " +
				"tweet VARCHAR(45) NOT NULL, " +
				"creationDate DATE, " +
				"stamp TIMESTAMP)";
		Connection conn = ds.getConnection();
		try {
			PreparedStatement ps = conn.prepareStatement("DROP TABLE client");
			ps.execute();
		} catch (Exception e) {
			
		}
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.execute();

		_sdi = new ClientDaoImpl();
		_sdi.setReadSql("select * from client where " +
				"((:id IS NOT NULL and id = :id) OR (:id IS NULL)) and " +
				"(:lastName IS NULL AND :firstName IS NULL OR lastName = :lastName and " +
				"firstName = :firstName)");
		_sdi.setListSql(LIST_CLIENT_SQL);
		_sdi.setInserSql("insert into client (firstName, " +
				"lastName, tweet, creationDate, stamp) VALUES (:firstName, " +
				":lastName, :tweet, CURRENT_DATE, CURRENT_TIMESTAMP)");
		_sdi.setUpdateSql("update client set firstName = :firstName, " +
				"lastName = :lastName, tweet = :tweet, stamp = CURRENT_TIMESTAMP " +
				"where id = :id and stamp = :stamp");
		_sdi.setDeleteSql("delete from client where id = :id and stamp = :stamp");

		_sdi.setDataSource(ds);

		_logger.info("Setup done");
		
		conn = ds.getConnection();
		ps = conn.prepareStatement("delete from client");
		ps = conn.prepareStatement("insert into client (firstName, " +
				"lastName, tweet, creationDate, stamp) VALUES ('f.n.', " +
				"'L.n.', 'tweet', CURRENT_DATE, CURRENT_TIMESTAMP)");
		ps.execute();
		insertClients();
	}

	@Test
	public void testList0() {
		_logger.info("List client where lastName contain 'L' ...");
		List<Client> cls = _sdi.listClient("%L%");
		assertTrue("Did not found any client", cls != null && cls.size() > 0);
		if (cls != null)
			for (Client cl : cls)
				_logger.info("Found client: " + cl.toString());
		_logger.info("List client where lastName contain 'L' done");
	}

	public void insertClients() {
		int count = 10;
		_logger.info(String.format("Inserting %d clients ...", count));
		long jj = System.currentTimeMillis();
		for (long ii = jj; ii <= jj + count; ii++) {
			Client clt = new Client();
			clt.setFirstName(ii + " F " + ii);
			clt.setLastName(ii + " L " + ii);
			clt.setTweet("hey " +ii);

			assertTrue("Failed to inserted client", _sdi.manageClient(DaOp.INSERT, clt));
			_insertedClientId = clt.getId();
		}
		_logger.info(String.format("Inserted %d clients done", count));
	}

	@Test
	public void testList() {
		_logger.info("List client where lastName contain 'L' ...");
		List<Client> cls = _sdi.listClient("%L%");
		assertTrue("Did not found any client", cls != null && cls.size() > 0);
		if (cls != null)
			for (Client cl : cls)
				_logger.info("Found client: " + cl.toString());
		_logger.info("List client where lastName contain 'L' done");
	}

	@Test
	public void testUpdate() {
		_logger.info(String.format("Update client %d ...", _insertedClientId));
		assertTrue("Missing new client ID", _insertedClientId != null);
		Client clt = _sdi.readClient(_insertedClientId);
		assertTrue("Missing client", clt != null);
		clt.setTweet("Tweet " + UUID.randomUUID());
		assertTrue("Failed to update client", _sdi.manageClient(DaOp.UPDATE, clt));
		clt = _sdi.readClient(clt.getId());
		assertTrue("Failed to read client", clt != null);
		_logger.info(String.format("Update client %d done", _insertedClientId));
	}

	@Test
	public void testDelete() {
		_logger.info(String.format("Delete client %d ...", _insertedClientId));
		Client clt = _sdi.readClient(_insertedClientId);
		assertTrue(String.format("Missing client with ID=%d", _insertedClientId), clt != null);
		assertTrue(String.format("Failed to delete client with ID=%d", _insertedClientId),
				_sdi.manageClient(DaOp.DELETE, clt));
		clt = _sdi.readClient(_insertedClientId);
		assertTrue(String.format("Client was not deleted ID=%d", _insertedClientId), clt == null);
		_logger.info(String.format("Delete client %d done", _insertedClientId));
	}
	@Test
	public void testListStream() {
		ClientDaoImpl cdao = new ClientDaoImpl() {
			@Override
			public List<Client> listClient(String lastName_) {
				final List<Client> clientList = new ArrayList<Client>();
				Client clt = new Client();
				clt.setLastName(lastName_);
				queryList("list", LIST_CLIENT_SQL, Client.class, new RowHandler<Client>() {
					@Override
					public void handleRow(Client row) {
						clientList.add(row);
					}
				}, clt);
				return clientList;
			}
		};
			
		cdao.setDataSource(ds);
		List<Client> result = cdao.listClient("%L%");
		assertEquals(12,result.size());
	}
	
	
	@Test
	public void testListIn() {
		ClientDaoImpl cdao = new ClientDaoImpl() {
			@Override
			public List<Client> listClient(String lastName_) {
				final List<Integer> clientList = new ArrayList<Integer>();
				clientList.add(12);
				clientList.add(1);
				return queryList("list", "select * from client where id in (:id)", Client.class,Collections.singletonMap("id", clientList));
			}
		};
			
		cdao.setDataSource(ds);
		List<Client> result = cdao.listClient(null);
		assertEquals(2,result.size());
	}

}
