/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dao.test.bean;

import java.util.Date;

public class Client {
	private Long id;
	private String firstName;
	private String lastName;
	private Date creationDate;
	private String stamp;
	private String tweet;
	public Long getId() {
		return id;
	}
	public void setId(Long id_) {
		id = id_;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName_) {
		firstName = firstName_;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName_) {
		lastName = lastName_;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate_) {
		creationDate = creationDate_;
	}
	public String getStamp() {
		return stamp;
	}
	public void setStamp(String stamp_) {
		stamp = stamp_;
	}
	public String getTweet() {
		return tweet;
	}
	public void setTweet(String tweet_) {
		tweet = tweet_;
	}
}
