/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.butor.json.service.BinResponseHandler;
import org.butor.json.service.ResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

public class BinStreamHandler implements StreamHandler<byte[]> {
	protected Logger _logger = LoggerFactory.getLogger(getClass());

	@Override
	public void parse(InputStream is,
			ResponseHandler<byte[]> handler, String loReqInfo) throws IOException {
		// if content-type and headers are set, the stream starts
		// with "___content_type___\n"
		String boundary = "___content_type___\n";
		BinResponseHandler brh = (BinResponseHandler)handler;
		OutputStream os = brh.getOutputStream();

		int n = 0;
		int bl = boundary.length();
		byte[] buffer = new byte[1024];
		while (n < bl) {
			byte b = (byte)is.read();
			if (b == -1) {
				break;
			}
			buffer[n++] = b;
		}
		
		String contentType = null;
		Map<String, String> headers = new HashMap<String, String>();
		if (new String(buffer, 0, bl).equalsIgnoreCase(boundary)) {
			// then retrieve content-type and others headers till reaching \n\n
			ByteBuffer bb = ByteBuffer.allocate(1024); // will grows if more space required
			bb.clear();
			byte prevB = 0, b=0;
			while (true) {
				prevB = b;
				b = (byte) is.read();
				if (b == 10 && prevB == 10) { // end of headers
					break;
				}
				bb.put(b);
			}
			bb.flip();
			String[] lines = new String(bb.array(), 0, bb.limit()).split("\n");
			bb.clear();
			for (String h : lines) {
				if (Strings.isNullOrEmpty(h)) {
					continue;
				}
				int pos = h.indexOf(":");
				String hn = h.substring(0, pos++);
				String hv = h.substring(pos);
				if (hn .equalsIgnoreCase("Content-Type")) {
					contentType = hv;
				} else {
					headers.put(hn, hv);
				}
			}

		} else {
			os.write(buffer, 0, bl);
		}

		// set contentType and headers when data start arriving
		// usefull if the client block the UI till data arrive
		boolean contentTypeSent = false;
		while ((n = is.read(buffer)) > -1) {
			if (!contentTypeSent && (contentType != null||headers != null)) {
				contentTypeSent = true;
				brh.setContentType(contentType, headers);
			}
			os.write(buffer, 0, n);
		}
		if (!contentTypeSent && (contentType != null||headers != null)) {
			contentTypeSent = true;
			brh.setContentType(contentType, headers);
		}
		brh.end();
	}

}
