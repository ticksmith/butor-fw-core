/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json;


public class JsonRequest extends CommonRequestArgs {
	private String service;
	private String serviceArgsJson;
	private boolean streaming = false;
	private int rowsPerChunk = 40;
	public String getService() {
		return service;
	}
	public void setService(String service_) {
		service = service_;
	}
	public String getServiceArgsJson() {
		return serviceArgsJson;
	}
	public void setServiceArgsJson(String serviceArgsJson_) {
		serviceArgsJson = serviceArgsJson_;
	}
	public boolean isStreaming() {
		return streaming;
	}
	public void setStreaming(boolean streaming_) {
		streaming = streaming_;
	}
	public int getRowsPerChunk() {
		return rowsPerChunk;
	}
	public void setRowsPerChunk(int rowsPerChunk) {
		this.rowsPerChunk = rowsPerChunk;
	}
	
	@Override
	public JsonRequest clone(){
		//CommonRequestArgs cra = super.clone();
		JsonRequest jr =  new JsonRequest(); 
		jr.setSessionId(this.getSessionId());
		jr.setDomain(this.getDomain());
		jr.setReqId(this.getReqId());
		jr.setUserId(this.getUserId());
		jr.setLang(this.getLang());
		jr.setService(this.getService());
		jr.setServiceArgsJson(this.getServiceArgsJson());
		jr.setStreaming(this.isStreaming());
		jr.setRowsPerChunk(this.getRowsPerChunk());
		return jr;
	}
}
