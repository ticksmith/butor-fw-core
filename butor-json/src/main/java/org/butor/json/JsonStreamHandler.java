/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;

import org.butor.json.service.ResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonStreamHandler<T> implements StreamHandler<T> {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	protected Logger loggerJsonRow = LoggerFactory.getLogger("jsonRowLogger");

	@Override
	public void parse(InputStream is,
			ResponseHandler<T> handler, String reqLogInfo) throws IOException {

		JsonHelper jsh = new JsonHelper();
		Type type = handler.getResponseType();
		if (type == null) {
			logger.error("Cannot determine the type for response handler {}!!!",handler);
		}

		int chunkCounter = -1;
		ByteBuffer bb = ByteBuffer.allocate(1024); // will grows if more space required
		bb.clear();
		while (true) {
			byte b = (byte) is.read();
			if (b == -1)
				break;

			if (b == 0) {
				if (++chunkCounter == 0) {
					continue;
				}

				bb.flip();
				String chunk = new String(bb.array(), 0, bb.limit());
				handleChunk(chunk, type, jsh, reqLogInfo, handler);
				bb.clear();
				continue;
			}

			if (chunkCounter >= 0)
				if (bb.remaining() == 0) {
					ByteBuffer tmp = ByteBuffer.allocate(bb.capacity() *2);
					tmp.clear();
					bb.flip();
					tmp.put(bb.array(), 0, bb.limit());
					bb = tmp;
				}
				bb.put(b);
		}
		handler.end();
	}

	/**
	 * Invoked when a message object (e.g: {@link ChannelBuffer}) was received
	 * from a remote peer.
	 */
	private void handleChunk(String chunk, Type type, JsonHelper jsh, String reqLogInfo, ResponseHandler<T> handler) {
		if (chunk.indexOf(Constants.RESPONSE_HEADER_CHUNK) > -1) {
			// header OK
			return;
		}
		if (chunk.indexOf(Constants.RESPONSE_MESSAGE_CHUNK) > -1) {
			logger.info("got message: {}, {}", reqLogInfo, chunk);
			JsonResponseMessage jrm = jsh.deserialize(chunk, JsonResponseMessage.class);
			handler.addMessage(jrm.getMessage());

		} else {
			loggerJsonRow.info("got row: {}, {}", reqLogInfo, chunk);
			if (type == null) {
				logger.error("We were not able to determine the rows type from the handler.addRow(type) method!");
				return;
			}
			T row = jsh.deserialize(chunk, type);
			handler.addRow(row);
		}
	}




}
