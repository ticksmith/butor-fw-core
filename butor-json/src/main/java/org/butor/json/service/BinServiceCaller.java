/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import org.butor.json.BinStreamHandler;
import org.butor.json.JsonServiceRequest;
import org.butor.json.StreamHandler;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpMediaType;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.MultipartContent;
import com.google.api.client.http.MultipartContent.Part;

public class BinServiceCaller<T> extends BaseServiceCaller<T> {

	private StreamHandlerFactory<?> streamHandlerFactory;

	public BinServiceCaller(String namespace, String url, StreamHandlerFactory<?> streamHandlerFactory) {
		super(namespace, url);
		this.streamHandlerFactory=streamHandlerFactory;
	}

	void setStreamHandlerFactory(StreamHandlerFactory<?> streamHandlerFactory) {
		this.streamHandlerFactory = streamHandlerFactory;
	}

	@Override
	public StreamHandler<?> getStreamHandler() {
		if (streamHandlerFactory == null) {
			return new BinStreamHandler();
		}
		return streamHandlerFactory.create();
	}
	
	@Override
	protected HttpContent getHttpContent(final JsonServiceRequest jsonServiceRequest, final ResponseHandler<?> handler, final GenericUrl url) {
		HttpContent content;
		//Send a MultipartContent in the presence of a InputStream in the Handler.  
		if (isDoMultiPartRequest() && handler instanceof BinResponseHandler && ((BinResponseHandler) handler).getInputStream() != null) {
			
			content = new MultipartContent().setMediaType(new HttpMediaType("multipart/form-data").setParameter("boundary", "__END_OF_PART__"));
			Part payloadPart = new Part(getAbstractHttpContent(jsonServiceRequest, url));			
			/*
			 * (non-Javadoc)
			 * Must use the name 'payload', so that Mule can recognized it as a payload.			
			 * @see org.mule.transport.http.HttpMultipartMuleMessageFactory#extractPayloadFromHttpRequest(HttpRequest)
			 */
			payloadPart.setHeaders(new HttpHeaders().set("Content-Disposition", String.format("form-data; name=\"%s\"", "payload")));
			((MultipartContent) content).addPart(payloadPart);

			Part attachementPart = new Part(new InputStreamContent("text/plain", ((BinResponseHandler) handler).getInputStream()));
			attachementPart.setHeaders(new HttpHeaders().set("Content-Disposition", String.format("form-data; name=\"%s\"", "attachment")));
			((MultipartContent) content).addPart(attachementPart);

		} else {
			content = getAbstractHttpContent(jsonServiceRequest, url);
		}
		return content;
	}
}
