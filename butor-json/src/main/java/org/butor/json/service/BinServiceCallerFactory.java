/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import java.util.Set;

import org.butor.json.StreamHandler;
import org.butor.json.service.binary.RowBasedBinStreamHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BinServiceCallerFactory<T> extends ServiceCallerFactory<T> {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	protected StreamHandlerFactory<?> streamHandlerFactory;

	public BinServiceCallerFactory(String namespace, String url) {
		this(namespace, url, -1, null);
	}
	public BinServiceCallerFactory(String namespace, String url, int maxPayloadLengthToLog) {
		this(namespace, url, -1, null);
	}
	public BinServiceCallerFactory(String namespace,
			String url, int maxPayloadLengthToLog, Set<String> servicesToNotLogArgs) {
		super(namespace, url, maxPayloadLengthToLog, servicesToNotLogArgs);
	}
	
	public BinServiceCallerFactory(String namespace, String url,
			StreamHandlerFactory<RowBasedBinStreamHandler> streamHandlerFactory) {
		this(namespace,url,-1,null);
		this.setStreamHandlerFactory(streamHandlerFactory);
	}
	public void setStreamHandlerFactory(StreamHandlerFactory<?> streamHandlerFactory) {
		this.streamHandlerFactory = streamHandlerFactory;
	}
	
	@Override
	protected ServiceCaller<T> getServiceCaller(String namespace, String url,
			int maxPayloadLengthToLog, Set<String> servicesToNotLogArgs) {
		BinServiceCaller<T> sc = new BinServiceCaller<T>(namespace, url,streamHandlerFactory);
		sc.setMaxPayloadLengthToLog(maxPayloadLengthToLog);
		sc.setServicesToNotLogArgs(servicesToNotLogArgs);
		return sc;
	}
	

}
