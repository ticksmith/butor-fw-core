/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.butor.json.util.JsonResponse;
import org.butor.utils.Message;
import org.butor.utils.Message.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class ResponseHandlerHelper {
	protected final static Logger LOGGER = LoggerFactory.getLogger(ResponseHandlerHelper.class);
	
	/**
	 * Use this method to send list to your ResponseHandler
	 * @param list
	 * @param handler
	 */
	public static <T>  void addList(List<T> list, ResponseHandler<T> handler) {
		Preconditions.checkNotNull(handler, "ResponseHandler should not be null");
		Preconditions.checkNotNull(list, "List should not be null");
		Message msg = new Message("totalRows", MessageType.DATA, Integer.toString(list.size()));
		handler.addMessage(msg);
		for (T o : list) {
			if (!handler.addRow(o))
				break;
		}
	}

	public static <T> JsonResponse<T> createJsonResponse(final Type responseType) {
		final List<Message> messages = Lists.newArrayList();
		final List<T> rows = Lists.newArrayList();
	
		JsonResponse<T> x = new JsonResponse<T>() {
			@Override
			public boolean addMessage(Message msg) {
				messages.add(msg);
				return true;
			}
	
			@Override
			public boolean addRow(T row) {
				rows.add(row);
				return true;
			}
	
			@Override
			public void end() {
			}
	
			@Override
			public List<Message> getMessages() {
				return messages;
			}
	
			@Override
			public List<T> getRows() {
				return rows;
			}
	
			@Override
			public T getRow() {
				if (rows != null && rows.size() > 0) {
					return rows.get(0);
				}
				return null;
			}
	
			@Override
			public Type getResponseType() {
				return responseType;
			}
		};
		return x;
	}
	
	
	public static <T> ResponseHandler<T> createJsonStreamingResponseHandler(final Type responseType, final OutputStream os) {
		final List<Message> messages = Lists.newArrayList();
	    final Gson gson = new Gson(); 
	
		ResponseHandler<T> x = new ResponseHandler<T>() {
			@Override
			public boolean addMessage(Message msg) {
				messages.add(msg);
				return true;
			}
	
			@Override
			public boolean addRow(T row) {
				try {
					os.write((gson.toJson(row, responseType)+"\n").getBytes(Charsets.UTF_8));
					
					return true;
				} catch (IOException e) {
					LOGGER.warn("Failed to addRow", e);
				}
				return false;
			}
	
			@Override
			public void end() {
				try {
					os.flush();
				} catch (IOException e) {
					LOGGER.warn("Failed to flush the output stream", e);
				}
			}
	

			
			@Override
			public Type getResponseType() {
				return responseType;
			}

		};
		return x;
	}
	
	public static BinResponseHandler createBinaryResponse(final InputStream is, final OutputStream os) {
		
		final Type byteArrayType =  new TypeToken<byte[]>(){

			private static final long serialVersionUID = 1L;}.getType();
		BinResponseHandler x = new BinResponseHandler() {
			
			@Override
			public Type getResponseType() {
				return byteArrayType;
			}
			
			@Override
			public void end() {
				// TODO Auto-generated method stub
			}
			
			@Override
			public boolean addRow(byte[] row_) {
				try {
					os.write(row_);
					os.flush();
					return true;
				} catch (IOException e) {
					LOGGER.warn("Failed to addRow", e);
				}
				return false;
			}
			
			@Override
			public boolean addMessage(Message message_) {
				return true;
			}
			
			@Override
			public void setContentType(String contentType, Map<String, String> headers) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public OutputStream getOutputStream() {
				return os;
			}

			@Override
			public InputStream getInputStream() {				
				return is;
			}
		};
		
		return x;
	}

}
