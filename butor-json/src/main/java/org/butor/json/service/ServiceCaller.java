/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import org.butor.json.JsonServiceRequest;

public interface ServiceCaller<T> {
	void call(JsonServiceRequest jsonServiceRequest_,
		ResponseHandler<?> handler_) throws ServiceCallException;
	JsonServiceRequest createRequest(String service_,
			Object serviceArgs_, String userId_, String sessionId_, String reqId);
	String serialize(Object args_);
}
