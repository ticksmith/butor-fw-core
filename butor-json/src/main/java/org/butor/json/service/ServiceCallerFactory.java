/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Set;

import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonServiceRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;

import com.google.common.base.Preconditions;
import com.google.common.reflect.Reflection;

/**
 * 
 * @param <SERVICE_INTERFACE> The service interface to create a proxy for
 * 
 */
public class ServiceCallerFactory<SERVICE_INTERFACE> implements FactoryBean<SERVICE_INTERFACE> {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private Class<SERVICE_INTERFACE> serviceInterface;
	private final String namespace;
	private final String url;
	private final int maxPayloadLengthToLog;
	private final Set<String> servicesToNotLogArgs;
	private int  requestReadTimeout=-1;
	private boolean doMultiPartRequest;
	

	public ServiceCallerFactory(String namespace, String url) {
		this(namespace, url, -1, null);
	}
	public ServiceCallerFactory(String namespace, String url, int maxPayloadLengthToLog) {
		this(namespace, url, maxPayloadLengthToLog, null);
	}
	
	public ServiceCallerFactory(String namespace, String url, 
			int maxPayloadLengthToLog, Set<String> servicesToNotLogArgs) {
		logger.info("Creating service caller: namespace={}, url={} ...", namespace, url);
		Preconditions.checkNotNull(namespace);
		Preconditions.checkNotNull(url);
		this.namespace = namespace;
		this.url=url;
		this.maxPayloadLengthToLog=maxPayloadLengthToLog;
		this.servicesToNotLogArgs=servicesToNotLogArgs;
	}
	
	public SERVICE_INTERFACE createServiceCaller(Class<SERVICE_INTERFACE> interface_) {
		final ServiceCaller<SERVICE_INTERFACE> serviceCaller = instanciateServiceCaller();
		
		InvocationHandler ih = new InvocationHandler() {
	
			@Override
			public Object invoke(Object proxy_, Method method_, Object[] args_)
					throws Throwable {
	
				Context<?> ctx = (Context<?>)args_[0];
	
				// remove context from args passed to service as json args
				Object[] serviceArgs = new Object[args_.length-1];
				for (int ii=1; ii<args_.length;ii++)
					serviceArgs[ii-1] = args_[ii];
				CommonRequestArgs request = ctx.getRequest();
				JsonServiceRequest jsr = null;
				if (request instanceof JsonServiceRequest) {
					jsr = (JsonServiceRequest)request;
				} else {
					jsr = serviceCaller.createRequest(
							"", null, request.getUserId(),request.getSessionId(), request.getReqId());
					jsr.setLang(request.getLang());
					jsr.setDomain(request.getDomain());
				}
				jsr.setService(method_.getName());
				jsr.setServiceArgsJson(serviceCaller.serialize(serviceArgs));
				jsr.setNamespace(namespace);
				
				serviceCaller.call(jsr, ctx.getResponseHandler());
	
				return null;
			}
		};
		return Reflection.newProxy(interface_, ih);
	}
	
	//FIXME where is this used and why? Looks really unrelated to what this class is intended for. 
	public <E> Context<E> getContext(
			final String userId_,
			final String sessionId_,
			final String reqId,
			final String lang_,
			final String domain,
			final ResponseHandler<E> handler_) {
	
		final ServiceCaller<SERVICE_INTERFACE> serviceCaller = instanciateServiceCaller();
	
		final JsonServiceRequest jsr = serviceCaller.createRequest(
				"", null, userId_, sessionId_, reqId);
		jsr.setDomain(domain);
	
		jsr.setLang(lang_);
		Context<E> ctx = new Context<E>() {
			@Override
			public ResponseHandler<E> getResponseHandler() {
				return handler_;
			}
			@Override
			public JsonServiceRequest getRequest() {
				return jsr;
			}
		};
		return ctx;
	}
	@Override
	public SERVICE_INTERFACE getObject() throws Exception {
		Preconditions.checkNotNull(serviceInterface, "Service interface is mandatory");
		return createServiceCaller(serviceInterface);
	}
	@Override
	public Class<?> getObjectType() {
		return serviceInterface;
	}
	/**
	 * Set the request timeout
	 * @param requestReadTimeout
	 */
	public void setRequestReadTimeout(int requestReadTimeout) {
		this.requestReadTimeout=requestReadTimeout;
	}
	public void setServiceInterface(Class<SERVICE_INTERFACE> serviceInterface) {
		this.serviceInterface = serviceInterface;
	}
	public void setDoMultiPartRequest(boolean doMultiPartRequest) {
		this.doMultiPartRequest = doMultiPartRequest;
		
	}
	@Override
	public boolean isSingleton() {
		return true;
	}
	protected ServiceCaller<SERVICE_INTERFACE> getServiceCaller(String namespace, String url,
			int maxPayloadLengthToLog, Set<String> servicesToNotLogArgs) {
		DefaultServiceCaller<SERVICE_INTERFACE> dsc = new DefaultServiceCaller<SERVICE_INTERFACE>(namespace, url);
		dsc.setMaxPayloadLengthToLog(maxPayloadLengthToLog);
		dsc.setServicesToNotLogArgs(servicesToNotLogArgs);
		return dsc;
	}
	
	protected ServiceCaller<SERVICE_INTERFACE> instanciateServiceCaller() {
		final ServiceCaller<SERVICE_INTERFACE> serviceCaller = getServiceCaller(namespace, url, maxPayloadLengthToLog, servicesToNotLogArgs);
		if (serviceCaller instanceof BaseServiceCaller) {
			if (requestReadTimeout > -1 ) {
				((BaseServiceCaller<SERVICE_INTERFACE>)serviceCaller).setRequestReadTimeout(requestReadTimeout);
			}
			((BaseServiceCaller<SERVICE_INTERFACE>)serviceCaller).setDoMultiPartRequest(doMultiPartRequest);
		}
		return serviceCaller;
	}
}
