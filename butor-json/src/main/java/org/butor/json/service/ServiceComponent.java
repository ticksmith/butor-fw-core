/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceComponent {
	protected Logger _logger = LoggerFactory.getLogger(getClass());
	private String namespace;
	private Object component;
	private boolean binary = false;

	private ConcurrentMap<String, Method> servicesMap = 
			new ConcurrentHashMap<String, Method>();
	
	public ServiceComponent(String namespace_, Object component_) {
		this(namespace_, component_, false);
	}
	public ServiceComponent(String namespace, Object component, boolean binary) {
		this.namespace = namespace;
		this.component = component;
		this.binary = binary;
		registerServices();
	}
	public boolean isBinary() {
		return this.binary;
	}
	private void registerServices() {
		Method[] methods = component.getClass().getMethods();
		if (methods.length == 0) {
			_logger.info(String.format("No methods in components! ns=%, scmp=%s", 
					namespace, component.getClass().getName()));
			return;
		}

		for (Method m : methods) {
			Class<?> rt = m.getReturnType();
			Class<?>[] pts = m.getParameterTypes();

			if (pts.length > 0 && pts[0].equals(Context.class) && rt.equals(void.class)) { 
				String key = buildKey(m.getName(), pts.length);
				if (servicesMap.containsKey(key)) {
					_logger.warn(String.format("method %s with %d args has been mapped already. Ignoring similar one!", m.getName(), Integer.valueOf(pts.length)));
					continue;
				}
				_logger.info(String.format("Discovered service=%s, ns=%s with %d args", 
						m.toString(), namespace, pts.length));
				servicesMap.put(key, m);
			}
		}
	}
	private String buildKey(String methodName_, int nbArgs_) {
		return String.format("%s.%d", methodName_, nbArgs_);
	}

	Method getService(String serviceName_, int nbOfArgs_) {
		return servicesMap.get(buildKey(serviceName_, nbOfArgs_));
	}
	public String getNamespace() {
		return namespace;
	}
	public Object getComponent() {
		return component;
	}
}
