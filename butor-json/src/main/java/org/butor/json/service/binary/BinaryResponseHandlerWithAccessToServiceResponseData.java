/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.service.binary;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.util.Map;

import org.butor.json.service.ResponseHandler;
import org.butor.utils.Message;

/**
 * Class that supports binary services, but instead of using an output stream, the data is passed around using a ByteBuffer
 * 
 */
public abstract class BinaryResponseHandlerWithAccessToServiceResponseData implements ResponseHandler<byte[]>{
	
	public abstract void setContentType(String contentType, Map<String, String> headers);
	
	
	@Override
	public boolean addRow(byte[] row_) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Type getResponseType() {
		return byte[].class;
	}

	@Override
	public boolean addMessage(Message message_) {
		return true;
	}

	
	abstract protected void doReadChannel(ReadableByteChannel channel) throws IOException;
	
}
