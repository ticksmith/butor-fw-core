/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.util;

import java.util.UUID;

import com.google.common.base.Strings;

import org.butor.json.CommonRequestArgs;
import org.springframework.beans.BeanUtils;

public class CommonRequestArgsFactory {

	private final CommonRequestArgs template;
	private boolean createRequestId = false;
	private boolean createSessionId = false;

	/**
	 * Create a factory using a template to copy the attributes from
	 * 
	 * @param template
	 */
	public CommonRequestArgsFactory(CommonRequestArgs template) {
		this.template = template;
	}

	/**
	 * Create a factory without a template, this will create a new empty 
	 * CommonRequestFactory. 
	 * 
	 * 
	 */
	public CommonRequestArgsFactory() {
		this(null);
	}

	/**
	 * This will create a new CommonRequestArgs.
	 * If a template is used, most of the attributes will be copied to the new CommonRequestArgs 
	 * 
	 * If the createRequestId() was called on this factory, or if a template was used a new the RequestId will be generated for 
	 * the CommonRequestArgs.
	 * 
	 *  
	 *  If the createSessionId() was called on this factory, the session id will be generated.
	 *  
	 * 
	 * @return
	 */
	public CommonRequestArgs create() {
		CommonRequestArgs cra = new CommonRequestArgs();
		if (template != null) {
			BeanUtils.copyProperties(template, cra);
		} 
		if (createRequestId || template != null) {
			cra.setReqId("R-" + UUID.randomUUID().toString());
		}
		if (createSessionId) {
			if (Strings.isNullOrEmpty(cra.getSessionId())){
				cra.setSessionId("session-" + UUID.randomUUID().toString());
			}
		}
		return cra;
	}

	public CommonRequestArgsFactory createNewReqId() {
		createRequestId=true;
		return this;
	}
	
	public CommonRequestArgsFactory createNewSessionIdIfNotSet() {
		createSessionId = true;
		return this;
	}

	@Override
	public String toString() {
		return "CommonRequestArgsFactory [template=" + template + "]";
	}


}
