/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json.util;

import com.google.common.base.Preconditions;

import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;

public class ContextBuilder<T> {
	
	private CommonRequestArgs cra; 
	private ResponseHandler<T> responseHandler;
	

	public ContextBuilder<T> setResponseHandler(ResponseHandler<T> responseHandler) {
		this.responseHandler =responseHandler;
		return this;
	}
	
	public  ContextBuilder<T> createCommonRequestArgs(String userId, String lang) {
		this.cra = new CommonRequestArgsFactory().createNewReqId().createNewSessionIdIfNotSet().create();
		this.cra.setUserId(userId);
		this.cra.setLang(lang);
		return this;
	}

	public  ContextBuilder<T> createCommonRequestArgs(String userId, String lang,
			String sessionId, String reqId, String domain) {
		this.cra = new CommonRequestArgsFactory().createNewReqId().createNewSessionIdIfNotSet().create();
		this.cra.setUserId(userId);
		this.cra.setLang(lang);
		this.cra.setReqId(reqId);
		this.cra.setSessionId(sessionId);
		this.cra.setDomain(domain);
		return this;
	}

	public  ContextBuilder<T> setCommonRequestArgs(CommonRequestArgs cra) {
		this.cra = cra;
		return this;
	}
	
	public Context<T> build() {
		Preconditions.checkNotNull(responseHandler, "Response Handler must be set");
		Preconditions.checkNotNull(cra, "Common Request Args must be set");
		
		return new Context<T>() {
			@Override
			public CommonRequestArgs getRequest() {
				return cra;
			}

			@Override
			public ResponseHandler<T> getResponseHandler() {
				return responseHandler;
			}
			
		};
		
	}
	
}

