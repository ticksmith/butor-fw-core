/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.butor.json.service.Context;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Iterables;
import com.google.gson.JsonParser;


public class TestJsonHelper {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Test
	public void testSerDes() {
		JsonHelper jh = new JsonHelper();
		Date now = new Date();
		String j = jh.serialize(now);
		logger.info("Serialized NOW:{}", j);
		Date d = jh.deserialize(j, Date.class);
		assertTrue(d.equals(now));
	}
	@Test
	public void testDes() {
		JsonHelper jh = new JsonHelper();
		String j = "\"2012-12-28T00:00:00.000Z\"";
		logger.info("Serialized date:{}", j);
		Date d = jh.deserialize(j, Date.class);
		assertNotNull(d);
	}
	
	private class EndTimeCriteria {
		private long endTime = -1;

		public long getEndTime() {
			return endTime;
		}

		public void setEndTime(long endTime) {
			this.endTime = endTime;
		}
	}
	//Inspired by http://stackoverflow.com/a/14369566/5024208	
	@Test
	public void testSerializeDeserialize () {
		Class<?>[] pts = new Class [] { Context.class, EndTimeCriteria.class };
		final Object[] serviceParameters = new Object[pts.length];
		
		String jsonArgs = "[{\"endTime\":20170208235959999}]";		
		Object[] params  = Iterables.toArray(new JsonParser().parse(jsonArgs).getAsJsonArray(), Object.class);
		JsonHelper jsHelper = new JsonHelper();
		for (int ii=1; ii<pts.length; ii++) {
			String par = params[ii-1].toString();			
			serviceParameters[ii] = jsHelper.deserialize(par, pts[ii]);
		}
		assertEquals(20170208235959999L, ((EndTimeCriteria)serviceParameters[1]).getEndTime());
	}	
}
