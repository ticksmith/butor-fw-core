/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.spring;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.util.ResourceUtils;

public class StringResourceFactoryBean implements FactoryBean<String>, BeanFactoryAware, InitializingBean {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private String convertedResource;
	private String charSet = null;
	private String defaultValue = null;
	private String resourcePaths = null;
	private ConfigurableListableBeanFactory beanFactory = null;
	private boolean resolvePlaceholders = true;

	public StringResourceFactoryBean(String resourcePaths) {
		this(resourcePaths, null, true);
	}

	public StringResourceFactoryBean(String resourcePaths, String charSet) {
		this(resourcePaths, charSet, true);
	}

	public StringResourceFactoryBean(String resourcePaths, boolean resolvePlaceholders) {
		this(resourcePaths, null, resolvePlaceholders);
	}

	public StringResourceFactoryBean(String resourcePaths, String charSet, boolean resolvePlaceholders) {
		this.charSet = charSet;
		this.resourcePaths = resourcePaths;
		this.resolvePlaceholders = resolvePlaceholders;
	}

	private String getResource(String resourcePath) {
			try (BufferedReader br = new BufferedReader(
					charSet == null ? new InputStreamReader(ResourceUtils.getURL(resourcePath).openStream())
							: new InputStreamReader(ResourceUtils.getURL(resourcePath).openStream(), charSet))) {
				StringBuilder sb = new StringBuilder();
				for (String line = br.readLine(); line != null; line = br.readLine()) {
					sb.append(line);
					sb.append('\n');
					
				}
				if (sb.length()>0) { // remove the last trailing \n
					sb.deleteCharAt(sb.length()-1);
				}
				return sb.toString();
			} catch (Exception e) {
				throw new IllegalArgumentException(String.format("Unable to read resource : %s (%s)", resourcePath,e.getMessage()), e);
			}
	}

	@Override
	public String getObject() throws Exception {
		if (convertedResource == null) {
			if (defaultValue != null) {
				logger.info("String from resource was null, Using default value");
				convertedResource = defaultValue;
			} else {
				throw new IllegalArgumentException(String.format("Unable to read resource from : %s", resourcePaths));
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("got resource : \"{}\"", convertedResource);
		}
		return convertedResource;
	}

	@Override
	public Class<?> getObjectType() {
		return String.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	/**
	 * If the default value is set and the file is not found, this is the value
	 * that will be returned.
	 * 
	 * @param defaultValue
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}



	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		if (beanFactory instanceof ConfigurableListableBeanFactory) {
			this.beanFactory = (ConfigurableListableBeanFactory) beanFactory;
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		String[] toks = resourcePaths.split(",");
		String res = null;
		for (String tok : toks) {
			try {
				logger.info("Getting resource from resourcePath={}, charSet={}...", tok, charSet);
				res = getResource(tok);
				break;
			} catch (IllegalArgumentException ex) {
				logger.warn("Failed to get resource from resourcePath={}! will try with next value if provided.", tok);
			}
		}
		convertedResource = res;
		// resolve embedded placeholders (if any)
		if (resolvePlaceholders && beanFactory != null && convertedResource != null) {
			convertedResource = beanFactory.resolveEmbeddedValue(convertedResource);
		}
	}

}
