/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.spring;
import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springtest.xml")
public class SpringTest {

	@Resource
	SimpleBean simple;
	
	@Resource
	SimpleBean checkTrim;
	

	@Resource
	SimpleBean checkMultiline;

	@Resource
	SimpleBean checkEmpty;

	


	@Test
	public void test() {
		assertNotNull(simple);
		assertTrue("placeholders was NOT resolved!", simple.getValue().indexOf("${") == -1);
	}
	
	@Test
	public void testWithTrim() {
		assertNotNull(checkTrim);
		assertEquals("test", checkTrim.getValue());
	}
	
	
	
	@Test
	public void testMultiline() {
		assertNotNull(checkMultiline);
		assertEquals("This is a multiline\nfile with\n3 lines", checkMultiline.getValue());
	}
	
	@Test
	public void testEmpty() {
		assertNotNull(checkEmpty);
		assertEquals("", checkEmpty.getValue());
	}
	
	
}
