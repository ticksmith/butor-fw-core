/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.spring;
import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:springtestResolve.xml")
public class SpringTestDefaultAndNoResolve {

	@Resource
	SimpleBean checkDefaultFilePresent;

	@Resource
	SimpleBean checkDefaultFileMissing;
	
	@Resource
	SimpleBean withoutResolve;

	

	
	@Test
	public void testWithoutResolve() {
		assertNotNull(withoutResolve);
		assertEquals("this is a test ${ph}", withoutResolve.getValue());
	}
	
	@Test
	public void testDefaultMissing() {
		assertNotNull(checkDefaultFileMissing);
		assertEquals("defaultValue", checkDefaultFileMissing.getValue());
	}

	@Test
	public void testDefaultPresent() {
		assertNotNull(checkDefaultFilePresent);
		assertEquals("this is a test 12", checkDefaultFilePresent.getValue());
	}

}
