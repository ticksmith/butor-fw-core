/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.checksum;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Preconditions;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * The checksum is generated like this : {@code CHECKSUM = SALT+MAXTIME+HASH} where SALT
 * = 8 bytes of random positive {@code Long JSON = JSON(Sorted Map<String,Object>}
 * representing the object) MAXTIME = Long representing the maximum valid period
 * for this checksum, (optional) {@code HASH = HASHFUNCTION(JSON+SALT+MAXTIME)} (MAXTIME
 * is Long.MAX_VALUE if no TTL)
 * 
 * 
 * 
 * @author tbussier
 * 
 */
public enum CommonChecksumFunction implements ChecksumFunction {
	MD5(Hashing.md5()), SHA1(Hashing.sha1()), SHA256(Hashing.sha256()), SHA512(
			Hashing.sha512());

	private final HashFunction hf;
	private final int checksumLengthWithoutTTL;
	private final int checksumLengthWithTTL;
	private final static Random random = new Random();

	private CommonChecksumFunction(HashFunction hf) {
		this.hf = hf;
		// simplification of ((hf.bits()/8)*2)+16*2.
		// divide by 8 to get the number of bytes, times 2 to get
		// the string length once it is converted to hexadecimal.
		// 16 is the length of the string in hexadecimal of a Long.
		// one Long for the maximum validation time

		this.checksumLengthWithoutTTL = hf.bits() / 4 + 16;
		this.checksumLengthWithTTL = checksumLengthWithoutTTL + 16;
	}

	@Override
	public String generateChecksum(String object) {
		return generateChecksum(object, createSalt(), Long.MAX_VALUE);
	}

	@Override
	public boolean validateChecksum(String object, String checksum) {
		if (!(checksum != null && object != null)) {
			return false;
		}
		;
		boolean includeTTL;
		if (checksum.length() == checksumLengthWithTTL) {
			includeTTL = true;
		} else if (checksum.length() == checksumLengthWithoutTTL) {
			includeTTL = false;
		} else {
			return false;
		}
		String saltStr;
		Long goodTill = Long.MAX_VALUE;
		if (includeTTL) {
			String timeStr = checksum.substring(0, 16);
			goodTill = Long.decode("0x" + timeStr);
			if (System.currentTimeMillis() > goodTill) {
				return false;
			}
			;
			saltStr = checksum.substring(16, 32);
		} else {
			saltStr = checksum.substring(0, 16);
		}
		Long salt = Long.decode("0x" + saltStr);
		String hash = generateChecksum(object, salt, goodTill);
		return hash.equals(checksum);
	}

	@Override
	public String generateChecksumWithTTL(String object, long duration,
			TimeUnit unit) {
		return generateChecksum(object, createSalt(),
				System.currentTimeMillis() + unit.toMillis(duration));
	}

	/**
	 * 
	 * @param object to checksum
	 * @param salt the slart part
	 * @return a check sum
	 */
	private String generateChecksum(String object, long salt, long maxTime) {
		Preconditions.checkNotNull(object,
				"Cannot generate checksum for null objects");
		// make sure the string is always 16 char long by left padding with '0'
		String saltStr = String.format("%16s", Long.toString(salt, 16))
				.replace(' ', '0');
		
		// if the maxTime is Long.MAX_VALUE we don't add the time to the checksum.
		String timeStr = maxTime == Long.MAX_VALUE ? "" : String.format("%16s",
				Long.toString(maxTime, 16)).replace(' ', '0');

		return timeStr
				+ saltStr
				+ hf.newHasher().putUnencodedChars(object).putLong(salt)
						.putLong(maxTime).hash().toString();
	}

	private static Long createSalt() {
		Long salt = Math.abs(random.nextLong());
		return salt;
	}
}
