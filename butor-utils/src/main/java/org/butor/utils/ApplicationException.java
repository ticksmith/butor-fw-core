/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.util.Arrays;

public class ApplicationException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -6147946187526379626L;

    final Message[] messages;
    private static final Message[]  EMPTY_MESSAGE_ARRAY = new Message[] {};
    
    
    
    public static RuntimeException exception(Throwable cause, Message... messages){
        return exception(null,cause,messages);
    }

    
    public static RuntimeException exception(String message, Message... messages){
        return exception(message,null,messages);
    }

    public static RuntimeException exception(Message... messages){
        return exception(null,null,messages);
        
    }
    
    public static RuntimeException exception(String message, Throwable cause, Message... messages){
        throw new ApplicationException(message,cause,messages);
    }


    private ApplicationException(String message, Throwable cause,Message... messages) {
        super(message, cause);
        this.messages = messages;
    }
    
    public Message[] getMessages() {
        if (messages == null) {
            return EMPTY_MESSAGE_ARRAY;
        }
        return messages;
    }


    @Override
    public String toString() {
        return String.format("Application Exception %s[%s]", StringUtil.isEmpty(getMessage()) ? "" : "("+getMessage()+") ",Arrays.toString(messages));
    }



}
