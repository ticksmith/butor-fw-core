/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.common.base.Throwables;
import com.google.common.collect.Maps;

public class ArgsBuilder {
	private Map<String, Object> args = Maps.newHashMap();
	
	/**
	 * @deprecated  As of release 1.0.7, please use ArgsBuilder.create()}
	 */
	@Deprecated
	public ArgsBuilder() {}
	/**
	 * This methods adds to the inner map all the field declared from the object 
	 * given in parameter.
	 * 
	 * @param value object to extract fields from
	 * @return ArgsBuilder (self)
	 */
	
	public ArgsBuilder addAllFieldsFromObject(Object value) {
		List<PropertyDescriptor> fields = new ArrayList<PropertyDescriptor>();
		for (Class<?> c = value.getClass(); c != null; c = c.getSuperclass()) {
			if (c.equals(Object.class))
				continue;
			for (Field f : c.getDeclaredFields()) {
				try {
					String propName = f.getName();
					PropertyDescriptor pd =  null;
					try {
						pd = new PropertyDescriptor(propName, c);
						if (pd != null) {
							Method readMethod = pd.getReadMethod();
							if (readMethod != null) {
								Object propertyValue = readMethod.invoke(value);
								set(propName,propertyValue);
							}
						}
					} catch (Exception e1) {
						//Swallow
					}
				} catch (Exception e) {
					Throwables.propagate(e);
				}	
			}
		}
		return this;
	}
	
	public ArgsBuilder set(String name, Object value) {
		args.put(name, value);
		return this;
	}
	public ArgsBuilder remove(String name) {
		args.remove(name);
		return this;
	}
	public Map<String, Object> build() {
		return args;
	}
	public static ArgsBuilder create() {
		return new ArgsBuilder();
	}
}
