/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.util.Map;

import com.google.common.collect.Maps;

public abstract class BeanWithAttributes {
	private String attributes;
	private Map<String, Object> attributesMap;
	public String getAttributes() {
		return attributes;
	}
	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}
	public Map<String, Object> getAttributesMap() {
		return attributesMap;
	}
	public void setAttributesMap(Map<String, Object> attributesMap) {
		this.attributesMap = attributesMap;
	}
	public void setAttribute(String key, Object value) {
		if (attributesMap == null) {
			attributesMap = Maps.newHashMap();
		}
		attributesMap.put(key, value);
	}
	public Object getAttribute(String key) {
		if (attributesMap == null) {
			return null;
		}
		return attributesMap.get(key);
	}
	public void removeAttribute(String key) {
		if (attributesMap != null) {
			attributesMap.remove(key);
		}
	}
}