/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.util.List;
import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class CSVWriter {

	private final List<Field> fields = Lists.newArrayList();
	private final Map<String, Integer> fieldsIndex = Maps.newHashMap();
	private final Map<String, String> fieldsDesc = Maps.newHashMap();
	private final String delim;
	private final String quoteChar;
	
	CSVWriter(List<Field> fields, String delim, String quoteChar) {
		this.fields.addAll(fields);
		int index = 0;
		this.delim = delim;
		this.quoteChar = quoteChar;
		for (Field f : fields) {
			fieldsIndex.put(f.header, index++);
			if (f.desc != null) {
				fieldsDesc.put(f.header, f.desc);
			}
		}
	}
	
	/**
	 * Get the header for this Writer
	 * 
	 * @return
	 */
	public String getHeader() {
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<fields.size();i++) {
			Field f = fields.get(i);
			String header = f.header;
			if (fieldsDesc.containsKey(header)) {
				header = fieldsDesc.get(header);
			}
			if (f.quoted) {
				sb.append(quoteChar);
				sb.append(header);
				sb.append(quoteChar);
			} else {
				sb.append(header);
			}
			if (!(i + 1 == fields.size())) {
				sb.append(delim);
			}
		}
		return sb.toString();
	}
	
	
	public int getFieldCount() {
		return fields.size();
	}
	
	/**
	 * Get the line using the String array provided as parameters;
	 * @param textArray
	 * @return
	 */
	public String getLine(String... textArray) {
		Preconditions.checkArgument(textArray.length == getFieldCount(),"Check array size");
		String quotedChar = "\\\\" + quoteChar;
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<textArray.length;i++) {
			String text = textArray[i];
			if (text != null) {
				Field f  = fields.get(i);
				if (f.quoted) {
					text = text.replaceAll(quoteChar, quotedChar);
					sb.append(quoteChar);
					sb.append(text);
					sb.append(quoteChar);
				} else {
					sb.append(text);
				}
			}
			if (!(i + 1 == fields.size())) {
				sb.append(delim);
			}
		}
		return sb.toString();
	}
	/**
	 * Get the line using the String array provided as parameters;
	 * @param textArray
	 * @return
	 */
	public String getLine(Map<String, String> row) {
		Preconditions.checkArgument(row != null,"Check bean != null");
		String quotedChar = "\\\\" + quoteChar;
		StringBuilder sb = new StringBuilder();
		int i=0;
		for (Field f : fields) {
			Object obj = row.get(f.header);
			if (obj == null) {
				obj = "";
			}
			String text = obj.toString();
			if (f.quoted) {
				text = text.replaceAll(quoteChar, quotedChar);
				sb.append(quoteChar);
				sb.append(text);
				sb.append(quoteChar);
			} else {
				sb.append(text);
			}

			i++;
			if (i < fields.size()) {
				sb.append(delim);
			}
		}
		return sb.toString();
	}

	/**
	 * 
	 * Class to configure the Fields in the CSV
	 * 
	 * @author tbussier
	 *
	 */
	static class Field {
		final String header;
		final boolean quoted;
		final String desc;
		
		Field(String header, boolean quoted) {
			this(header, null, quoted);
		}
		Field(String header, String desc, boolean quoted) {
			super();
			this.header = header;
			this.quoted = quoted;
			this.desc = desc;
		}
		
	}
}