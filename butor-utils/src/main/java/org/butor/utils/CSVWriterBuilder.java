/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.util.List;

import org.butor.utils.CSVWriter.Field;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class CSVWriterBuilder {
	
	private final List<Field> fields = Lists.newArrayList();

	private String delim = ";";
	private String quoteChar = "\"";
	
	/**
	 * Add an unquoted header
	 * 
	 * @param header
	 * @return
	 */
	public  CSVWriterBuilder addHeader(String header) {
		addHeader(header, null, false);
		return this;
	}
	
	/**
	 * Add an header
	 * 
	 * @param header
	 * @param quotedColumn
	 * @return
	 */
	public  CSVWriterBuilder addHeader(String header, String desc, boolean quotedColumn) {
		fields.add(new CSVWriter.Field(header, desc, quotedColumn));
		return this;
	}

	/**
	 * 
	 * Set the quote character, this will be escaped in the String to print and quoted fields
	 * will be surrounded by this character
	 * 
	 * Default to '"'
	 * 
	 * @param quoteChar
	 * @return
	 */
	public  CSVWriterBuilder setQuoteCharacter(String quoteChar) {
		Preconditions.checkArgument(!Strings.isNullOrEmpty(quoteChar),"QuoteChar is mandatory");
		this.quoteChar= quoteChar; 
		return this;
	}
	/**
	 * Set the field delimiter
	 * 
	 * Default to ';'
	 * 
	 * @param delim
	 * @return
	 */
	public CSVWriterBuilder setDelimiter(String delim) {
		Preconditions.checkArgument(!Strings.isNullOrEmpty(delim),"QuoteChar is mandatory");
		this.delim= delim; 
		return this;
	}

	/**
	 * Build a CSVWriter.
	 * 
	 * @return
	 */
	public  CSVWriter build() {
		return new CSVWriter(fields, delim, quoteChar);
	}
}