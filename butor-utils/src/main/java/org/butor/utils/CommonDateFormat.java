/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public enum CommonDateFormat {
	HHMM("HH:mm"),
	HHMMSS("HH:mm:ss"),
	YYYYMMDD("yyyy-MM-dd"),
	YYYYMMDD_COMPACT("yyyyMMdd"),
	YYYYMMDD_HHMM("yyyy-MM-dd HH:mm"),
	YYYYMMDD_HHMM_COMPACT("yyyyMMddHHmm"),
	YYYYMMDD_HHMMSS("yyyy-MM-dd HH:mm:ss"),
	YYYYMMDD_HHMMSS_COMPACT("yyyyMMddHHmmss"),
	YYYYMMDD_HHMMSS_WITHMS("yyyy-MM-dd HH:mm:ss.SSS"),
	YYYYMMDD_HHMMSS_WITHMS_COMPACT("yyyyMMddHHmmssSSS"),
	ISO8601("yyyy-MM-dd'T'HH:mm:ss'Z'",TimeZone.getTimeZone("UTC")),
	ISO8601_WITHMS("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",TimeZone.getTimeZone("UTC"));
	
	private final String pattern;
	private final TimeZone tz;
	
	private CommonDateFormat(String pattern) {
		this(pattern,TimeZone.getDefault());
	}
	
	private CommonDateFormat(String pattern,TimeZone tz) {
		this.pattern=pattern;
		this.tz=tz;
	}
	
	
	private  ThreadLocal<SimpleDateFormat> tl = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			sdf.setLenient(false);
			sdf.setTimeZone(tz);
			return sdf;
		}
	};
	
	
	public Date parse(String date) throws ParseException {
		return tl.get().parse(date);
	}
	
	public String format(Date date) {
		return tl.get().format(date);
	}
}
