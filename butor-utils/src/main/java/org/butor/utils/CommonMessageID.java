/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import org.butor.utils.Message;
import org.butor.utils.Message.MessageType;
import org.butor.utils.MessageID;

public enum CommonMessageID implements MessageID {
	SERVICE_NOT_AVAILABLE(MessageType.ERROR),
	SUCCESS(MessageType.INFO),
	SUCCESSFULL_UPDATE(MessageType.INFO),
	SERVICE_TIMEOUT(MessageType.ERROR),
	SERVICE_FAILURE(MessageType.ERROR),
	MISSING_ARG(MessageType.ERROR),
	MISSING_CONFIG(MessageType.ERROR),
	INVALID_ARG(MessageType.ERROR),
	NOT_FOUND(MessageType.INFO);

	private String sysId = "common";
	private final MessageType type;
	private final Message messageObject;

	private CommonMessageID(MessageType type) {
		this.type = type;
		this.messageObject =new Message(this); 
	}

	public Message getMessage() {
		return messageObject;
	}

	public Message getMessage(String message) {
		return new Message(this, message);
	}

	@Override
	public String getId() {
		return name();
	}


	@Override
	public String getSysId() {
		return this.sysId;
	}
	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	@Override
	public MessageType getMessageType() {
		return type;
	}
}
