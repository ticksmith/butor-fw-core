/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DigestHelper {
	private static Logger _logger = LoggerFactory.getLogger(DigestHelper.class.getName());
	
	public static final String SYSTEM_PROPERTY_REALM_ENCRYPTION_ALGORITHM = "butor_realm_encryption_algorithm";
	public static final String ALGORITHM_SHA1 = "SHA1";
	public static final String PROPERTY_ENCRYPTION_ALGORITHM = "encryption_algorithm";

	
	public static String encrypt(String str, String algo) {
		if (str == null) {
			_logger.warn("Got null string to encrypt!");
			return null;
		}
		
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance(algo);
		} catch (NoSuchAlgorithmException e) {
			_logger.error("Failed", e);
			return null;
		}
		
		try {
			md.update(str.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			_logger.error("Failed", e);
			return null;
		}
		
		byte raw[] = md.digest();
		String hash = encodeLikeSunMiscBASE64Encoder(raw);
		
		return hash;
	}
	
	/**
	    * Returns a string in the hexadecimal format.
	    * 
	    * @param bytes the converted bytes
	    * @return the hexadecimal string representing the bytes data
	    * @throws IllegalArgumentException if the byte array is null
	    */
	   public static String toHexString(byte[] bytes)
	   {
	      if (bytes == null)
	      {
	         throw new IllegalArgumentException("byte array must not be null");
	      }
	      StringBuffer hex = new StringBuffer(bytes.length * 2);
	      for (int i = 0; i < bytes.length; i++)
	      {
	         hex.append(Character.forDigit((bytes[i] & 0XF0) >> 4, 16));
	         hex.append(Character.forDigit((bytes[i] & 0X0F), 16));
	      }
	      return hex.toString();
	   }

	private static final int SUN_MISC_BASE64ENCODER_MAX_LINE_LENGTH = 76;

	private static String encodeLikeSunMiscBASE64Encoder(byte[] bytes) {
		StringBuilder result = new StringBuilder();
		String encoded = DatatypeConverter.printBase64Binary(bytes);
		for (int i = 0; i < encoded.length(); i += SUN_MISC_BASE64ENCODER_MAX_LINE_LENGTH) {
			if (i != 0) {
				result.append('\n');
			}
			result.append(encoded.substring(i, Math.min(encoded.length(), i + SUN_MISC_BASE64ENCODER_MAX_LINE_LENGTH)));
		}
		if (encoded.length() % SUN_MISC_BASE64ENCODER_MAX_LINE_LENGTH == 0 && !encoded.endsWith("=")) {
			result.append('\n');
		}
		return result.toString();
	}

}
