/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 *
 * @author sawanai
 * @date Aug 20, 2006
 */
public abstract class FileHelper {
	private static Logger _logger = LoggerFactory.getLogger(FileHelper.class.getName());

	public static void copyFile(File src, File dst) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(src);
			os = new FileOutputStream(dst);
			while (is.available() > 0) {
				os.write(is.read());
			}
			
		} finally {
			if (is != null) {
				is.close();
			}
			if (os != null) {
				os.close();
			}
		}
	}
	
	public static boolean deleteFile(File file) {
		if (file == null) {
			return false;
		}
		// delete folder recursively
		if (file.isDirectory()) {
			// delete folder content first
			File[] files = file.listFiles();
			for (int ii=0; ii<files.length; ii++) {
				if (!deleteFile(files[ii])) {
					return false;
				}
			}
		}
		
		// delete file or empty folder
		return file.delete();
	}

	/**
	 * Given a user name and the name of a password file,
	 * this method returns the password in the file.  The
	 * username is only used to log if an error happen while
	 * trying to read the file.
	 *
	 * @param passwordFile Name of the file containing the
	 * password of user pstrUser
	 *
	 * @return The password stored in the password file or null if
	 * the file could not be read.
	 */
	public static String getPasswordFromFile(String passwordFile) {

		_logger.info("Getting pwd from file [" + passwordFile + "] ...");

		String strPassword = null;

		if (null == passwordFile) {
			_logger.warn("Got NULL password filename!");
			return null;
		}

		try {
			FileReader objPasswordFile = new FileReader(passwordFile);
			BufferedReader objPasswordReader = new BufferedReader(objPasswordFile);

			strPassword = objPasswordReader.readLine();
			if (null != strPassword) {
				strPassword = strPassword.trim();
			} else {
				_logger.warn("No password in file [" + passwordFile + "]");
			}

			objPasswordFile.close();
		} catch (FileNotFoundException e) {
			_logger.error("Failed", e);
		} catch (IOException e) {
			_logger.error("Failed", e);
		}

		return strPassword;
	}
}
