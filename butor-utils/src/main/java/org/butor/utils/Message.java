/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;
/**
 * TODO
 * 
 * @author sawanai
 * Jul 1, 2004
 */
public class Message {
    	
    	public enum MessageType {
    		DATA,
    	    INFO,
    	    WARNING,
    	    ERROR
    	}
	
	private final String id;
	private String sysId;
	private final MessageType type;
	private final String message;
	
	public String getId() {
		return id;
	}

	public String getMessage() {
		return message;
	}
	
	public Message(MessageID messageID) {
		this(messageID,null);
	};

	public Message(MessageID messageID, String message_) {
		id = messageID.getId();
		sysId = messageID.getSysId();
		type = messageID.getMessageType();
		message = message_;
	};
	
	public Message(int id_, MessageType type_, String message_) {
		this(String.valueOf(id_), type_, message_, null);
	}
	public Message(String id_, MessageType type_, String message_) {
		this(id_, type_, message_, null);
	}
	public Message(String id, MessageType type, String message, String sysId) {
		this.id = id;
		this.type = type;
		this.message = message;
		this.sysId = sysId;
	}

	public Message(String message_) {
		this(0, MessageType.INFO, message_);
	}
	

	@Override
    public String toString() {
		return String.format("{id:%s, type:%s, message:%s, sysid:%s}",id,type,message,sysId);
	}


	public MessageType getType() {
		return type;
	}

	public Message setSysId(String sysId) {
		this.sysId = sysId;
		return this;
	}
	public String getSysId() {
		return sysId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((sysId == null) ? 0 : sysId.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (sysId == null) {
			if (other.sysId != null)
				return false;
		} else if (!sysId.equals(other.sysId))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
}
