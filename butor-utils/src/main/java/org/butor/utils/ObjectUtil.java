/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

/**
 * Utility class for Objects
 * 
 * @author jdemers
 * 
 */
public class ObjectUtil {

	/**
	 * Most of the time, we want to use {@code o1.equals(o2)} with objects of the same type. For instance:
	 * 
	 * {@code return aDate.equals(foo.getTime());}
	 * 
	 * In our example, foo.getTime() returns a Date, which is fine. Lets say we refactor the code and decide getTime() will return a Long (millis) instead. The
	 * code above is now broken (it always return false), but it will compile without error and it will also execute without throwing exception at runtime. That
	 * kind of bug can be very costly to find and fix. It would be great if the compiler could tell us that we are using incompatible types. The function below
	 * does just that.
	 * 
	 * Static typing is a great feature of the Java language. However, IMHO the function {@code Object.equals(Object)} was badly designed from the very beginning of
	 * Java and we now have to live with it. We do not need to have the method equals() and hascode() in the Object class. Classes that support that feature
	 * should implement the interface Equalable (just like Comparable).
	 * 
	 * @param <T> an object
	 * @param <U> an object extends T
	 * 
	 * @param o1 object
	 * @param o2 object
	 * @return boolean true if o1 equals o2 and are of the same type, false otherwise
	 */
	public static <T, U extends T> boolean eq(T o1, U o2) {
		return o1 == null || o2 == null ? o1 == null && o2 == null : o1.equals(o2);
	}

}
