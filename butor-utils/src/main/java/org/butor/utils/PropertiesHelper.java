/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesHelper {
	private static Logger _logger = LoggerFactory.getLogger(PropertiesHelper.class.getName());
	private PropertiesHelper() {
	}

	public static Properties loadProperties(File file) {
		if (file == null) {
			_logger.warn("Got null  file name!");
			return null;
		}
		
		if (!file.exists()) {
			_logger.warn("File does not exists! file=[" +file.getPath() +"]");
			return null;
		}
		
		Properties props = new Properties();
		
		FileInputStream fis = null;
		try {
			// create page
			fis = new FileInputStream(file);
			props.load(fis);
			
		} catch (Exception e) {
			_logger.error("Failed", e);
			return null;
			
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception e) {
					//OK
				}
			}
		}
		
		return props;
	}

	public static boolean saveProperties(Properties props, File file) {
		if (props == null) {
			_logger.error("Got null Properties!");
			return false;
		}

		if (file == null) {
			_logger.error("Got null file name!");
			return false;
		}
		
		FileOutputStream fos = null;
		try {
			// create page
			fos = new FileOutputStream(file);
			
			props.store(fos,"");
			
		} catch (Exception e) {
			_logger.error("Failed", e);
			return false;
			
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (Exception e) {
					//OK
				}
			}
		}
		
		return true;
	}
}
