/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

public class StringUtil {
	public static boolean isEmpty(String str_) {
		return str_ == null || str_.trim().length() == 0;
	}
	/**
	 * Encodes a string for display in an XML page, replacing
	 * the usual reserved characters by the corresponding entities.
	 * @param s The plaintext
	 * @return The encoded string
	 */
	public static String xmlEncode(String s) {
		return s.replaceAll("&", "&amp;")  // must be first!
			    .replaceAll("<", "&lt;")
			    .replaceAll(">", "&gt;")
			    .replaceAll("'", "&apos;")
			    .replaceAll("\"", "&quot;");
	}
	
	/**
	 * Encodes a string for display in an HTML page, replacing
	 * the usual reserved characters by the corresponding entities.
	 * 
	 * Note that HTML does not recognize the XML entity for ' (single quote),
	 * and that, in particular, IE does not replace {@code &apos;} by single quote!
	 * So xmlEncode() should not be used to prepare strings for display
	 * in an HTML page.
	 * 
	 * @param s The plaintext
	 * @return The encoded string
	 */
	public static String htmlEncode(String s) {
		return s.replaceAll("&", "&amp;")  // must be first!
			    .replaceAll("<", "&lt;")
			    .replaceAll(">", "&gt;")
				.replaceAll("\"", "&quot;");
	}
		
	/**
	 * Encodes a string for output as a JavaScript string, by escaping the
	 * quotes and double quotes.
	 * 
	 * @param s The plaintext
	 * @return The encoded string
	 */
	public static String javascriptEncode(String s) {
		return s.replaceAll("\"", "\\\\\"")
		        .replaceAll("'", "\\\\'");
	}
		
	/**
	 * Performs URL encoding.
	 * @param s The plaintext string
	 * @return The encoded string
	 */
	public static String urlEncode(String s) {
		try {
			return java.net.URLEncoder.encode(s, "UTF-8");
		}
		catch (Exception e) {
			// will never occur - UTF-8 is always supported
			return s;
		}
	}
	
	/**
	 * Performs URL decoding.
	 * @param s The encoded string
	 * @return The plaintext string
	 */
	public static String urlDecode(String s) {
		try {
			return java.net.URLDecoder.decode(s, "UTF-8");
		}
		catch (Exception e) {
			// will never occur - UTF-8 is always supported
			return s;
		}
	}
	
}
