/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.zip;

import java.util.zip.ZipEntry;

public class ZipEntryInfo extends ZipUtils {
	boolean isDirectory = false;
	String name = null;
	int method = -1;
	long size = -1;
	long compressedSize = -1;
	
	public ZipEntryInfo() {
		super();
	}

	public String toString() {
		StringBuffer sb = new StringBuffer(name);
		if (isDirectory) {
			sb.append(" d ");
		} else {
			sb.append(" f ");
		}

		if (method == ZipEntry.STORED) {
			sb.append("stored   ");
		} else {
			sb.append("defalted ");
		}

		sb.append(" sz/csz=");
		sb.append(size);
		if (method == ZipEntry.DEFLATED) {
			sb.append("/" + compressedSize);
		}

		return sb.toString();
	}

	public long getCompressedSize() {
		return compressedSize;
	}

	public void setCompressedSize(long compressedSize) {
		this.compressedSize = compressedSize;
	}

	public boolean isDirectory() {
		return isDirectory;
	}

	public void setDirectory(boolean isDirectory) {
		this.isDirectory = isDirectory;
	}

	public int getMethod() {
		return method;
	}

	public void setMethod(int method) {
		this.method = method;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}
}
