/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.zip;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipEntryInputStream extends InputStream {

	InputStream is = null;
	ZipFile zf = null;
	int available = -1;
	
	public ZipEntryInputStream(File file, String entryName) throws IOException {
		super();
		zf = new ZipFile(file);
		ZipEntry ze = zf.getEntry(entryName);
		if (ze == null) {
			return;
		}
		is = zf.getInputStream(ze);
		available = (int)ze.getSize();
	}

	public int read() throws IOException {
		if (is == null) {
			return -1;
		}
		int b = is.read();
		if (b != -1) {
			available--;
		} else {
			available = 0;
		}
		return b;
	}
	
	public int read(byte[] bytes) throws IOException {
		if (is == null) {
			return -1;
		}
		int chunkLen = is.read(bytes);
		available -= chunkLen;
		return chunkLen;
	}
	
	public int read(byte[] bytes, int off, int len) throws IOException {
		if (is == null) {
			return -1;
		}

		int chunkLen = is.read(bytes, off, len);
		available -= chunkLen;
		return chunkLen;
	}
	
	public void close() throws IOException {
		if (is != null) {
			is.close();
		}
	}
	public int available() throws IOException {
		return available;
	}
}
