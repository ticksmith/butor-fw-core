/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.CharBuffer;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.butor.utils.IWriteListener;


public class ZipUtils {

	public ZipUtils() {
		super();
	}

	/**
	 * @param args[zip-file-name, entry-name]
	 * @throws Exception an IOException, ZipException
	 */
	public static void main(String[] args) throws Exception {
		File zipFile = new File(args[0]); 
		String entryName = args[1]; 
		List list = listContent(zipFile);
		if (list != null) {
			Iterator it = list.iterator();
			while (it.hasNext()) {
				System.out.println((ZipEntryInfo)it.next());
			}
		}
		
		File file = new File(entryName);
		String name = file.getName();
		System.out.println("Get bytes of " +name +":");
		byte[] bytes = getResourceBytes(zipFile, name);
		System.out.println(new String(bytes));
		
		System.out.println("Get InputStream of " +name +":");
		try {
			bytes = new byte[128];
			InputStream zeis = getResourceInputStream(zipFile, name);
			while (zeis.available() > 0) {
				int chunk = zeis.read(bytes);
				System.out.print(new String(bytes, 0, chunk));
			}
			zeis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		removeResource(zipFile, name);
	}

	public static List listContent(File file) throws ZipException {
		ZipFile zf = null;
		try {
			List list = new LinkedList();
			// extracts just sizes only.
			zf = new ZipFile(file);
			Enumeration e = zf.entries();
			while (e.hasMoreElements()) {
				ZipEntry ze = (ZipEntry) e.nextElement();
				ZipEntryInfo zei = new ZipEntryInfo();
				zei.setName(ze.getName());
				zei.setDirectory(ze.isDirectory());
				zei.setMethod(ze.getMethod());
				zei.setSize(ze.getSize());
				zei.setCompressedSize(ze.getCompressedSize());
				list.add(zei);
			}
			zf.close();
			zf = null;
			return list;
			
		} catch (Exception e) {
			throw new ZipException(e.getMessage());
			
		} finally  {
			try {
				if (zf != null) {
					zf.close();
				}
			} catch (Exception e2) {
				throw new ZipException(e2.getMessage());
			}
		}
	}
	public static byte[] getResourceBytes(File file, String resName) throws ZipException {
		ZipFile zf = null;
		InputStream is = null;
		try {
			zf = new ZipFile(file);
			ZipEntry ze = zf.getEntry(resName);
			if (ze == null) {
				return null;
			}
			is = zf.getInputStream(ze);
			int size = (int)ze.getSize();
			if (size == -1) {
				return null;
			}
			byte[] bytes = new byte[size];
			int rb = 0;
			while (true) {
				int chunk = is.read(bytes, rb, (int)(size -rb));
				if (chunk <= 0) {
					break;
				}
				rb += chunk;
			}
			is.close();
			is = null;
			zf.close();
			zf = null;			
			return bytes;

		} catch (Exception e) {
			throw new ZipException(e.getMessage());
			
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			try {
				if (zf != null) {
					zf.close();
				}
			} catch (IOException e2) {
				throw new ZipException(e2.getMessage());
			}
		}
	}
	public static CharBuffer getResourceChars(File file, String resName, String charsetName) throws ZipException {
		ZipFile zf = null;
		InputStreamReader isr = null;
		try {
			zf = new ZipFile(file);
			ZipEntry ze = zf.getEntry(resName);
			if (ze == null) {
				return null;
			}
			if (charsetName != null) {
				isr = new InputStreamReader(zf.getInputStream(ze), charsetName);
			} else {
				isr = new InputStreamReader(zf.getInputStream(ze));
			}
			
			int size = (int)ze.getSize();
			if (size <= 0) {
				return null;
			}
			CharBuffer cb = CharBuffer.allocate(size);
			char[] buffer = new char[1024];
			int rb = 0;
			while (true) {
				//int chunk = isr.read(cb.array(), rb, (int)(size -rb));
				int chunk = isr.read(buffer);
				if (chunk <= 0) {
					break;
				}
				rb += chunk;
				cb.put(buffer, 0, chunk);
			}
			isr.close();
			isr = null;
			zf.close();
			zf = null;
			
			return cb;

		} catch (Exception e) {
			throw new ZipException(e.getMessage());
			
		} finally {
			try {
				if (isr != null) {
					isr.close();
				}
			} catch (IOException e2) {
				throw new ZipException(e2.getMessage());
			}
			
			try {
				if (zf != null) {
					zf.close();
				}
			} catch (IOException e2) {
				throw new ZipException(e2.getMessage());
			}
		}
	}	
	public static InputStream getResourceInputStream(File file, String entryName) throws IOException {
		return new ZipEntryInputStream(file, entryName);
	}	
	
	public static int putResource(File file, String entryName, InputStream fis, long lastModified) throws ZipException {
		return putResource(file, entryName, fis, lastModified, null);
	}
	public static int putResource(File file, String entryName, InputStream fis, long lastModified, IWriteListener writeListener) throws ZipException {
			if (fis == null) {
				return -1;
			}
			
			boolean completed = false; 
			ZipFile zf = null;
			File tmpFile = null;			
			ZipOutputStream zos = null;
			try {
				// copy the content of the archive toa temporary file and skip the file to add if it exists
				if (file.exists()) {
					tmpFile = new File(file.getPath() +".part");
					zos = new ZipOutputStream(new FileOutputStream(tmpFile));			
					
					zf = new ZipFile(file);
					Enumeration e = zf.entries();
					while (e.hasMoreElements()) {
						ZipEntry ze = (ZipEntry) e.nextElement();
						if (ze.getName().equals(entryName)) {
							continue; // skip this entry (to be added
						}
						zos.putNextEntry(ze);
						
						InputStream is = zf.getInputStream(ze);
						int size = (int)ze.getSize();
						if (size > 0) {
							byte[] bytes = new byte[128];
							int rb = 0;
							while (true) {
								int chunk = is.read(bytes);
								if (chunk <= 0) {
									break;
								}
								rb += chunk;
								zos.write(bytes, 0, chunk);
							}
						}
						is.close();
						zos.closeEntry();					
					}
					zf.close();
					zf = null;
					
				} else {
					zos = new ZipOutputStream(new FileOutputStream(file));
				}
				
				byte[] bytes = new byte[1024];
	
				ZipEntry ze = new ZipEntry(entryName);
				ze.setMethod(ZipOutputStream.DEFLATED);
				ze.setTime(lastModified);
	
				// Add the zip entry and associated data.
				zos.putNextEntry(ze);

				if (writeListener != null) {
					writeListener.writeStarted(fis.available());
				}
				int resourceSize = 0;
				int chunk = 0;
				while ((chunk = fis.read(bytes)) > 0) {
					resourceSize += chunk;
					zos.write(bytes, 0, chunk);
					if (writeListener != null) {
						writeListener.writeChunkSize(chunk);
					}
				}
				completed = true;
				
				zos.closeEntry();
				zos.finish();
				zos.close();
				zos = null;
				
				if (tmpFile != null) {
					file.delete();
					tmpFile.renameTo(file);
				}
				return resourceSize;
				
			} catch (Exception e) {
				if (!completed && writeListener != null) {
					writeListener.writeAborted();
				}
				throw new ZipException(e.getMessage());
				
			} finally {
				if (completed && writeListener != null) {
					writeListener.writeCompleted();
				}
				try {
					if (zf != null) {
						zf.close();
					}
				} catch (IOException e2) {
					throw new ZipException(e2.getMessage());
				}

				try {
					if (zos != null) {
						zos.close();
					}
				} catch (IOException e2) {
					throw new ZipException(e2.getMessage());
				}
			}
		}

	public static boolean removeResource(File file, String entryName) throws ZipException {
		if (!file.exists()) {
			return false;
		}

		ZipFile zf = null;
		boolean foundEntry = false;
		try {
			// check if zip contain the file to remove
			zf = new ZipFile(file);
			Enumeration e = zf.entries();
			while (e.hasMoreElements()) {
				ZipEntry ze = (ZipEntry) e.nextElement();
				if (ze.getName().equals(entryName)) {
					foundEntry = true;
					break;
				}
			}
			zf.close();
			zf = null;
			
		} catch (Exception e) {
			throw new ZipException(e.getMessage());
			
		} finally {
			try {
				if (zf != null) {
					zf.close();
				}
			} catch (IOException e2) {
				throw new ZipException(e2.getMessage());
			}
		}
		
		if (!foundEntry) {
			throw new ZipException("Zip Entry Not found [" +entryName +"]");
		}
		
		File tmpFile = null;
		ZipOutputStream zos = null;
		try {
			// copy the content of the archive toa temporary file and skip the file to add if it exists
			tmpFile = new File(file.getPath()+".part");
			zos = new ZipOutputStream(new FileOutputStream(tmpFile));			
			
			zf = new ZipFile(file);
			Enumeration e = zf.entries();
			while (e.hasMoreElements()) {
				ZipEntry ze = (ZipEntry) e.nextElement();
				if (ze.getName().equals(entryName)) {
					continue; // skip this entry (to be added
				}
				zos.putNextEntry(ze);
				
				InputStream is = zf.getInputStream(ze);
				int size = (int)ze.getSize();
				if (size > 0) {
					byte[] bytes = new byte[128];
					int rb = 0;
					while (true) {
						int chunk = is.read(bytes);
						if (chunk <= 0) {
							break;
						}
						rb += chunk;
						zos.write(bytes, 0, chunk);
					}
				}
				is.close();
				zos.closeEntry();					
			}
			zf.close();
			zf = null;
			
			zos.closeEntry();
			zos.finish();
			zos.close();
			zos = null;
			
			file.delete();
			tmpFile.renameTo(file);
			return true;
			
		} catch (Exception e) {
			throw new ZipException(e.getMessage());
			
		} finally {
			try {
				if (zf != null) {
					zf.close();
				}
			} catch (IOException e2) {
				throw new ZipException(e2.getMessage());
			}

			try {
				if (zos != null) {
					zos.close();
				}
			} catch (IOException e2) {
				throw new ZipException(e2.getMessage());
			}
		}
	}
	
	public static int putResource(File file, File fileToAdd, String entryName) throws ZipException {
		return putResource(file, fileToAdd, entryName, null);
	}
	public static int putResource(File file, File fileToAdd, String entryName, IWriteListener writeListener) throws ZipException {
		if (!fileToAdd.exists()) {
			return -1;
		}
		
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileToAdd);
			return putResource(file, entryName, fis, fileToAdd.lastModified(), writeListener);
			
		} catch (IOException e) {
			throw new ZipException(e.getMessage());
			
		} finally {
			if(fis != null) {
				try {
					fis.close();
				} catch (IOException e2) {
					throw new ZipException(e2.getMessage());
				}
			}
		}
	}
}
