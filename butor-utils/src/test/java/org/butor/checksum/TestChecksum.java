/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.checksum;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.butor.checksum.ChecksumFunction;
import org.butor.checksum.CommonChecksumFunction;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TestChecksum {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Test
	public void testChecksum() {
		
		for (int i=0;i<100;i++) {
			for (ChecksumFunction checksumFunction : CommonChecksumFunction.values()) {
				String objectStr = "titi";
				String checksum = checksumFunction.generateChecksum(objectStr);
				logger.info(checksumFunction+" "+checksum);
				assertTrue(checksumFunction.validateChecksum(objectStr, checksum));
				String badObjectStr = "toto";
				assertFalse(checksumFunction.validateChecksum(badObjectStr, checksum));
			}
		}
	}
	@Test
	public void testTimeChecksum() {
		String objectStr="titi";
		for (ChecksumFunction checksumFunction : CommonChecksumFunction.values()) {
			String checksum = checksumFunction.generateChecksumWithTTL(objectStr,1,TimeUnit.MINUTES);
			assertTrue(checksumFunction.validateChecksum(objectStr, checksum));
		}
		
		for (ChecksumFunction checksumFunction : CommonChecksumFunction.values()) {
			String checksum = checksumFunction.generateChecksumWithTTL(objectStr,10,TimeUnit.MILLISECONDS);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
			assertFalse(checksumFunction.validateChecksum(objectStr, checksum));
		}
	
	}
	
	@Test
	public void testChecksumSHA256() {
		assertTrue(CommonChecksumFunction.SHA256.validateChecksum("allo1234", "4d0443e8b7f50fda64cd9842d632d804bacd54b118c7705f9c6ab09c3fda98dbba1c68a1c1f7e70b"));
	}
	

	@SuppressWarnings("unused")
	private static  class Bean {
		final int intValue;
		final double doubleValue;
		final String stringValue;
		public Bean(int intValue, double doubleValue, String stringValue) {
			super();
			this.intValue = intValue;
			this.doubleValue = doubleValue;
			this.stringValue = stringValue;
		}


		
	}

}
