/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.ldap;

import java.io.IOException;

public class TestLdapUser {
	private static String username;
	private static String pwd;
	private static String ldapUrl = "ldap://10.100.0.1:389";
	private static String domain = "IAGTO.CA";
	private static String searchBase = "DC=IAGTO,DC=CA";
	private static LdapUserModel ldap;

	public static void main(String[] args) throws IOException {
		byte[] b = new byte[100];
		System.out.println("Testing user authentification...");
		int c;
		System.out.print("Enter ldap username (asawan): ");
		c = System.in.read(b);
		if (c == 1) {
			username = "asawan";
		} else {
			username = new String(b, 0, c-1);
		}
		System.out.print("Enter " +username +" pwd: ");
		c = System.in.read(b);
		pwd = new String(b, 0, c-1);

		ldap = new DefaultLdapUserModel(ldapUrl, domain);
		LdapUser user = ldap.auth(username, pwd);
		System.out.println(user);

		System.out.println("Testing user search ...");
		DefaultLdapUserModel dldap = (DefaultLdapUserModel)ldap;
		dldap.setAdminUsername(username);
		dldap.setAdminPwd(pwd);
		dldap.setSearchBase(searchBase);
		System.out.print("Enter ldap user search: ");
		c = System.in.read(b);
		String name = new String(b, 0, c-1);
		System.out.println(ldap.search(name));
	}
}
