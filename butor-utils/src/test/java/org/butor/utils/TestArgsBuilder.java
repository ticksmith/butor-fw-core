/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

public class TestArgsBuilder {
	
	@Test
	public void testArgBuilder() {
		SampleCriteria sc = new SampleCriteria();
		sc.setCri1("test");
		sc.setD(2.0);
		
		Map<String, Object> m = ArgsBuilder.create().addAllFieldsFromObject(sc).build();
		assertEquals((Double)2.0,(Double)m.get("d"));
		assertEquals("test",m.get("cri1"));
	}
	
	@Test
	public void testArgBuilderAnonymous() {
		SampleCriteria sc2 = new SampleCriteria() {
			private String t2 = "t2";
		};
		sc2.setCri1("test");
		sc2.setD(2.0);
		Map<String, Object> m = ArgsBuilder.create().addAllFieldsFromObject(sc2).build();
		assertEquals((Double) 2.0, (Double) m.get("d"));
		assertEquals("test", m.get("cri1"));
		assertNull(m.get("t2"));
		
		SampleCriteria sc3 = new SampleCriteria() {
			private String t2 = "t2";
			public String getT2() {
				return t2;
			}
		};
		sc3.setCri1("test");
		sc3.setD(2.0);
		m = ArgsBuilder.create().addAllFieldsFromObject(sc3).build();
		assertEquals((Double) 2.0, (Double) m.get("d"));
		assertEquals("test", m.get("cri1"));
		assertNull(m.get("t2"));
	}

}
