/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.utils;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.Test;

public class TestDiffUtils {

	@Test
	public void testSimilarObjects() {
		DummyBean a = new DummyBean();
		a.a="Titi";
		a.i=2;
		a.p = BigDecimal.valueOf(1.0);
		a.z=new Integer(1);
		a.db=a;

		DummyBean c = new DummyBean();
		c.a="Titi";
		c.i=2;
		c.p= BigDecimal.valueOf(1.0);
		c.z=new Integer(1);
		c.db=a;
	
		assertEquals(c,a);
		
		DummyBean b = new DummyBean();
		b.b="Ohoh";
		b.i=2;
		b.z=new Integer(1);
		b.db=c;
		
		assertEquals(DiffUtils.getDifferences(a, b),DiffUtils.getDifferences(b,a));
		String[] fieldsThatDiffers = new ArrayList<String>(DiffUtils.getDifferences(a, b)).toArray(new String[]{}) ;
		assertArrayEquals(new String[] {"a","b","p"}, fieldsThatDiffers);
	}

	@Test
	public void testDifferentObjects() {
		DummyBean a = new DummyBean();
		a.b="No";
		
		DummyBean2 b = new DummyBean2();
		b.b="Yo";
		b.c="Yes";

		DummyBean2 c = new DummyBean2();
		c.b="Yo";
		c.c="No";
		
		String[] fieldsThatDiffers = new ArrayList<String>(DiffUtils.getDifferences(c, a)).toArray(new String[]{}) ;
		assertArrayEquals(new String[] {"b","c","d", "i"}, fieldsThatDiffers);
		assertEquals(DiffUtils.getDifferences(c,a),DiffUtils.getDifferences(b,a));
		assertEquals(DiffUtils.getDifferences(a, c),DiffUtils.getDifferences(a,b));
		
		c.c=null;
		fieldsThatDiffers = new ArrayList<String>(DiffUtils.getDifferences(c, a)).toArray(new String[]{}) ;
		assertArrayEquals(new String[] {"b","d", "i"}, fieldsThatDiffers);
		
	}
	@SuppressWarnings("unused")
	private class DummyBean2 {
		String b;
		String c;
	}

	private class DummyBean {
		
		String a;
		String b;
		Integer z;
		int i;
		double d;
		BigDecimal p;
		DummyBean db;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((a == null) ? 0 : a.hashCode());
			result = prime * result + ((b == null) ? 0 : b.hashCode());
			long temp;
			temp = Double.doubleToLongBits(d);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result + ((db == null) ? 0 : db.hashCode());
			result = prime * result + i;
			result = prime * result + ((p == null) ? 0 : p.hashCode());
			result = prime * result + ((z == null) ? 0 : z.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DummyBean other = (DummyBean) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (a == null) {
				if (other.a != null)
					return false;
			} else if (!a.equals(other.a))
				return false;
			if (b == null) {
				if (other.b != null)
					return false;
			} else if (!b.equals(other.b))
				return false;
			if (Double.doubleToLongBits(d) != Double.doubleToLongBits(other.d))
				return false;
			if (db == null) {
				if (other.db != null)
					return false;
			} else if (!db.equals(other.db))
				return false;
			if (i != other.i)
				return false;
			if (p == null) {
				if (other.p != null)
					return false;
			} else if (!p.equals(other.p))
				return false;
			if (z == null) {
				if (other.z != null)
					return false;
			} else if (!z.equals(other.z))
				return false;
			return true;
		}
		private TestDiffUtils getOuterType() {
			return TestDiffUtils.this;
		}
	}
}
